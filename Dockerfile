FROM node:5.4.1

RUN apt-get update
RUN apt-get install -y curl vim
RUN npm install -y -g bower http-server

COPY ./ snifme-web

WORKDIR snifme-web
RUN /bin/bash -c 'npm install'
RUN /bin/bash -c 'bower install --allow-root'
CMD ["npm","run","start-prod"]

# docker build -t mlennie/snifme-web . && docker stop web1 web2 && docker rm web1 web2 && docker run -d --net mynetwork --name web1 mlennie/snifme-web && docker run -d --net mynetwork --name web2 mlennie/snifme-web
# docker run -it --rm mlennie/snifme-web bash

#docker network create mynetwork
#docker run --net mynetwork --name weba -d node sh -c 'npm install http-server -g && mkdir -p /public && echo "welcome to weba" > /public/index.html && http-server -a 0.0.0.0 -p 4200'
#docker run --net mynetwork --name webb -d node sh -c 'npm install http-server -g && mkdir -p /public && echo "welcome to webb" > /public/index.html && http-server -a 0.0.0.0 -p 4200'

