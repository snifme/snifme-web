System.register(['angular2/core', 'angular2/router', '../../../models/logs/log.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, log_service_1;
    var LoginLinksComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (log_service_1_1) {
                log_service_1 = log_service_1_1;
            }],
        execute: function() {
            LoginLinksComponent = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function LoginLinksComponent(log) {
                    this.log = log;
                    this.showRegister = false;
                    this.showLogin = false;
                    this.showCancel = false;
                    this.showReconfirm = false;
                    this.showPassword = false;
                }
                /*************************************
                 * INITIALIZER
                 *************************************/
                LoginLinksComponent.prototype.ngOnInit = function () {
                    this.checkRegister();
                    this.checkLogin();
                    this.checkCancel();
                    this.checkReconfirm();
                    this.checkPassword();
                };
                /*************************************
                 * METHODS
                 *************************************/
                LoginLinksComponent.prototype.checkRegister = function () {
                    if (this.page !== 'register') {
                        this.showRegister = true;
                    }
                };
                LoginLinksComponent.prototype.checkLogin = function () {
                    if (this.page !== 'login') {
                        this.showLogin = true;
                    }
                };
                LoginLinksComponent.prototype.checkCancel = function () {
                };
                LoginLinksComponent.prototype.checkReconfirm = function () {
                    if (this.page !== 'reconfirm') {
                        this.showReconfirm = true;
                    }
                };
                LoginLinksComponent.prototype.checkPassword = function () {
                    if (this.page !== 'passwordResetRequest') {
                        this.showPassword = true;
                    }
                };
                LoginLinksComponent = __decorate([
                    core_1.Component({
                        selector: 'login-links',
                        templateUrl: 'app/pages/account/login-links/login-links.component.html',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        inputs: ['page']
                    }), 
                    __metadata('design:paramtypes', [log_service_1.LogService])
                ], LoginLinksComponent);
                return LoginLinksComponent;
            })();
            exports_1("LoginLinksComponent", LoginLinksComponent);
        }
    }
});
//# sourceMappingURL=login-links.component.js.map