import {Component, OnInit} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import {LogService} from '../../../models/logs/log.service';

@Component({
  selector: 'login-links',
  templateUrl: 'app/pages/account/login-links/login-links.component.html',
  directives: [ROUTER_DIRECTIVES],
  inputs: ['page']
})

export class LoginLinksComponent implements OnInit {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private log: LogService
  ) {}

  /*************************************
   * PROPERTIES
   *************************************/
  page: string;
  showRegister: boolean = false;
  showLogin: boolean = false;
  showCancel: boolean = false;
  showReconfirm: boolean = false;
  showPassword: boolean = false;

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
    this.checkRegister();
    this.checkLogin();
    this.checkCancel();
    this.checkReconfirm();
    this.checkPassword();
  }

  /*************************************
   * METHODS
   *************************************/
  checkRegister(){
    if (this.page !== 'register') {
      this.showRegister = true;
    }
  }

  checkLogin(){
    if (this.page !== 'login') {
      this.showLogin = true;
    }

  }

  checkCancel(){

  }

  checkReconfirm(){
    if (this.page !== 'reconfirm') {
      this.showReconfirm = true;
    }
  }

  checkPassword(){
    if (this.page !== 'passwordResetRequest') {
      this.showPassword = true;
    }
  }

}
