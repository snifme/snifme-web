System.register(['angular2/core', '../../form-errors/form-errors.component', '../login-links/login-links.component', '../../../utils/ajax.service', '../../../models/logs/log.service', '../../../utils/session.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, form_errors_component_1, login_links_component_1, ajax_service_1, log_service_1, session_service_1;
    var ReconfirmComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (form_errors_component_1_1) {
                form_errors_component_1 = form_errors_component_1_1;
            },
            function (login_links_component_1_1) {
                login_links_component_1 = login_links_component_1_1;
            },
            function (ajax_service_1_1) {
                ajax_service_1 = ajax_service_1_1;
            },
            function (log_service_1_1) {
                log_service_1 = log_service_1_1;
            },
            function (session_service_1_1) {
                session_service_1 = session_service_1_1;
            }],
        execute: function() {
            ReconfirmComponent = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function ReconfirmComponent(_ajaxService, log, _session) {
                    this._ajaxService = _ajaxService;
                    this.log = log;
                    this._session = _session;
                    /*************************************
                     * PROPERTIES
                     *************************************/
                    this.submitted = false;
                    this.linksPage = "reconfirm";
                    this.errors = {};
                    this.showErrorMessage = false;
                    this.sending = false;
                }
                /*************************************
                 * INITIALIZER
                 *************************************/
                ReconfirmComponent.prototype.ngOnInit = function () {
                    this.log.pageView("/reconfirm-account");
                    this._session.redirectIfLoggedIn();
                };
                /*************************************
                 * METHODS
                 *************************************/
                ReconfirmComponent.prototype.onSubmit = function () {
                    var _this = this;
                    var log = this.log;
                    log.formSubmit("Resend Confirmation");
                    this.sending = true;
                    this.errors = {};
                    this.showErrorMessage = false;
                    var options = {
                        data: { email: this.email },
                        path: 'resend-confirmation',
                        action: 'GET'
                    };
                    _this._ajaxService.send(options)
                        .then(function () {
                        _this.sending = false;
                        _this.submitted = true;
                        log.emailSend("Account Confirmation");
                    })
                        .catch(function (err) {
                        _this.sending = false;
                        if (err &&
                            err.responseJSON &&
                            err.responseJSON.errors) {
                            _this.errors.email = [err.responseJSON.errors];
                            log.error("Resend Confirmation Email", err.responseJSON.errors);
                        }
                        else {
                            log.error("Resend Confirmation Email", err);
                            _this.showErrorMessage = true;
                        }
                    });
                };
                ReconfirmComponent = __decorate([
                    core_1.Component({
                        selector: 'reconfirm-account',
                        templateUrl: 'app/pages/account/reconfirm/reconfirm.component.html',
                        directives: [login_links_component_1.LoginLinksComponent, form_errors_component_1.FormErrorsComponent]
                    }), 
                    __metadata('design:paramtypes', [ajax_service_1.AjaxService, log_service_1.LogService, session_service_1.SessionService])
                ], ReconfirmComponent);
                return ReconfirmComponent;
            })();
            exports_1("ReconfirmComponent", ReconfirmComponent);
        }
    }
});
//# sourceMappingURL=reconfirm.component.js.map