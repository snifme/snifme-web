import {Component,OnInit} from 'angular2/core';
import {Router} from 'angular2/router';
import {NgForm}           from 'angular2/common';
import {User}             from '../../../models/users/user';
import {FormErrorsComponent} from '../../form-errors/form-errors.component';
import {LoginLinksComponent} from '../login-links/login-links.component';
import {FacebookConnectComponent} from '../facebook-connect/facebook-connect.component';
import {AjaxService} from '../../../utils/ajax.service';
import {LogService} from '../../../models/logs/log.service';
import {SessionService} from '../../../utils/session.service';

@Component({
  selector: 'register-form',
  templateUrl: 'app/pages/account/register/register.component.html',
  directives: [
    LoginLinksComponent,
    FormErrorsComponent,
    FacebookConnectComponent
  ]
})

export class RegisterComponent implements OnInit {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private log: LogService,
    private _ajaxService:AjaxService,
    private _session: SessionService,
    private _router: Router
  ) {}

  // properties
  linksPage:string = "register";
  model = new User();
  showErrorMessage: boolean = false;
  sending:boolean = false;
  errors = {};
  submitted = false;

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
    this.log.pageView("/register");
    this._session.redirectIfLoggedIn();
  }

  private handleErrors = (err) => {
    this.sending = false;
    if (err &&
        err.responseJSON &&
        err.responseJSON.errors &&
        typeof err.responseJSON.errors === 'object' &&
        Object.keys(err.responseJSON.errors).length > 0) {

      const errors = err.responseJSON.errors;
      this.log.error("Registration Fail", errors);

      for (let name in errors) {
        if (errors[name][0] == "has already been taken") {
          errors[name] = [
            "Email has already been taken." +
            " If you previously registered with Facebook, " +
            "please use the \"Connect with Facebook\" button."
          ];
        }
        this.errors[name] = errors[name];
      }
    } else {
      this.showErrorMessage = true;
      this.log.error("Registration Fail", err);
    }
  }

  private handleNewUser = (newUser) => {
    this.log.account("Register", "Email and Password");
    this.log.register(newUser.user.id);
    this.log.profileSet(newUser);
    this.sending = false;
    this.submitted = true;
  }

  public onSubmit() {
    const _this = this;
    this.log.formSubmit("Register");
    this.sending = true;
    this.errors = {};
    this.showErrorMessage = false;
    this.model.save()
    .then(this.handleNewUser)
    .catch(this.handleErrors);
  }

}
