System.register(['angular2/core', 'angular2/router', '../../../models/users/user', '../../form-errors/form-errors.component', '../login-links/login-links.component', '../facebook-connect/facebook-connect.component', '../../../utils/ajax.service', '../../../models/logs/log.service', '../../../utils/session.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, user_1, form_errors_component_1, login_links_component_1, facebook_connect_component_1, ajax_service_1, log_service_1, session_service_1;
    var RegisterComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (user_1_1) {
                user_1 = user_1_1;
            },
            function (form_errors_component_1_1) {
                form_errors_component_1 = form_errors_component_1_1;
            },
            function (login_links_component_1_1) {
                login_links_component_1 = login_links_component_1_1;
            },
            function (facebook_connect_component_1_1) {
                facebook_connect_component_1 = facebook_connect_component_1_1;
            },
            function (ajax_service_1_1) {
                ajax_service_1 = ajax_service_1_1;
            },
            function (log_service_1_1) {
                log_service_1 = log_service_1_1;
            },
            function (session_service_1_1) {
                session_service_1 = session_service_1_1;
            }],
        execute: function() {
            RegisterComponent = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function RegisterComponent(log, _ajaxService, _session, _router) {
                    var _this = this;
                    this.log = log;
                    this._ajaxService = _ajaxService;
                    this._session = _session;
                    this._router = _router;
                    // properties
                    this.linksPage = "register";
                    this.model = new user_1.User();
                    this.showErrorMessage = false;
                    this.sending = false;
                    this.errors = {};
                    this.submitted = false;
                    this.handleErrors = function (err) {
                        _this.sending = false;
                        if (err &&
                            err.responseJSON &&
                            err.responseJSON.errors &&
                            typeof err.responseJSON.errors === 'object' &&
                            Object.keys(err.responseJSON.errors).length > 0) {
                            var errors = err.responseJSON.errors;
                            _this.log.error("Registration Fail", errors);
                            for (var name_1 in errors) {
                                if (errors[name_1][0] == "has already been taken") {
                                    errors[name_1] = [
                                        "Email has already been taken." +
                                            " If you previously registered with Facebook, " +
                                            "please use the \"Connect with Facebook\" button."
                                    ];
                                }
                                _this.errors[name_1] = errors[name_1];
                            }
                        }
                        else {
                            _this.showErrorMessage = true;
                            _this.log.error("Registration Fail", err);
                        }
                    };
                    this.handleNewUser = function (newUser) {
                        _this.log.account("Register", "Email and Password");
                        _this.log.register(newUser.user.id);
                        _this.log.profileSet(newUser);
                        _this.sending = false;
                        _this.submitted = true;
                    };
                }
                /*************************************
                 * INITIALIZER
                 *************************************/
                RegisterComponent.prototype.ngOnInit = function () {
                    this.log.pageView("/register");
                    this._session.redirectIfLoggedIn();
                };
                RegisterComponent.prototype.onSubmit = function () {
                    var _this = this;
                    this.log.formSubmit("Register");
                    this.sending = true;
                    this.errors = {};
                    this.showErrorMessage = false;
                    this.model.save()
                        .then(this.handleNewUser)
                        .catch(this.handleErrors);
                };
                RegisterComponent = __decorate([
                    core_1.Component({
                        selector: 'register-form',
                        templateUrl: 'app/pages/account/register/register.component.html',
                        directives: [
                            login_links_component_1.LoginLinksComponent,
                            form_errors_component_1.FormErrorsComponent,
                            facebook_connect_component_1.FacebookConnectComponent
                        ]
                    }), 
                    __metadata('design:paramtypes', [log_service_1.LogService, ajax_service_1.AjaxService, session_service_1.SessionService, router_1.Router])
                ], RegisterComponent);
                return RegisterComponent;
            })();
            exports_1("RegisterComponent", RegisterComponent);
        }
    }
});
//# sourceMappingURL=register.component.js.map