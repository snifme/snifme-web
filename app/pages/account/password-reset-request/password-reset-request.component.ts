import {Component, OnInit} from 'angular2/core';
import {FormErrorsComponent} from '../../form-errors/form-errors.component';
import {LoginLinksComponent} from '../login-links/login-links.component';
import {AjaxService} from '../../../utils/ajax.service';
import {LogService} from '../../../models/logs/log.service';
import {SessionService} from '../../../utils/session.service';

@Component({
  selector: 'password-reset-request',
  templateUrl: 'app/pages/account/password-reset-request/password-reset-request.component.html',
  directives: [LoginLinksComponent,FormErrorsComponent]
})

export class PasswordResetRequestComponent implements OnInit {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private _ajaxService:AjaxService,
    private log: LogService,
    private _session: SessionService
  ) {}

  /************************************* * PROPERTIES
   *************************************/
  submitted:boolean = false;
  email:string;
  linksPage:string = "passwordResetRequest";
  errors = {};
  showErrorMessage:boolean = false;
  sending:boolean = false;

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
    this.log.pageView("/password-reset-email");
    this._session.redirectIfLoggedIn();
  }

  /*************************************
   * METHODS
   *************************************/
  onSubmit() {
    const _this = this;
    const log = this.log;
    log.formSubmit("Request Password Reset Email");
    this.sending = true;
    this.errors = {};
    this.showErrorMessage = false;

    const options = {
      data: {email: this.email},
      path: 'password-reset-request',
      action: 'GET'
    };

    _this._ajaxService.send(options)
    .then(() => {
      _this.sending = false;
      _this.submitted = true;
      log.emailSend("Password Reset");
    })
    .catch((err) => {
      _this.sending = false;
      if (err &&
          err.responseJSON &&
          err.responseJSON.errors) {
          log.error("Password Reset Email Request", err.responseJSON.errors);
          _this.errors.email = [err.responseJSON.errors];
      } else {
          log.error("Password Reset Email Request",err);
        _this.showErrorMessage = true;
      }

    });
  }
}
