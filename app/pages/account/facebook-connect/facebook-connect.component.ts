import {Component,OnInit} from 'angular2/core';
import {Router} from 'angular2/router';
import {LogService} from '../../../models/logs/log.service';
import {AjaxService} from '../../../utils/ajax.service';
import {SessionService} from '../../../utils/session.service';
import {FacebookService} from '../../../utils/facebook.service';

@Component({
  selector: 'facebook-connect',
  templateUrl: 'app/pages/account/facebook-connect/facebook-connect.component.html'
})

export class FacebookConnectComponent implements OnInit {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private _ajaxService:AjaxService,
    private log: LogService,
    private _router: Router,
    private _session: SessionService,
    private _facebook: FacebookService
  ){}

  /*************************************
   * PROPERTIES
   *************************************/
  showFacebookErrorMessage: boolean = false;
  errors = {};
  sending: boolean = false;

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
  }

  /*************************************
   * METHODS
   *************************************/

  private handleFail = (err) => {
    this.sending = false;
    this.log.error("Connect with Facebook Fail", err);
    this.showFacebookErrorMessage = true;
  }

  private handleNewUser = (response) => {
    localStorage.session = JSON.stringify(response);
    this.sending = false;
    if (response["user_account_create"]) {
      this.log.account("Register", "With Facebook");
      const params = {register: "true"};
      this.log.register(response.user_id);
    } else {
      const params = {login: "true"};
    }
    this.log.account("Login", "With Facebook");
    this.log.identify();
    $('#session-check').trigger('click');
    this._router.navigate(['Home', params]);
  }

  private sendToAPI = (response) => {
    const options = {
       data: JSON.stringify(response),
       path: 'facebook-connect',
       action: 'POST'
     };
    return this._ajaxService.send(options);
  }


  public facebookLogin() {
    const _this = this;
    const log = this.log;
    this.sending = true;
    this.errors = {};
    this.showErrorMessage = false;
    this.showFacebookErrorMessage = false;
    this._facebook.connect()
    .then(this.sendToAPI)
    .then(this.handleNewUser)
    .catch(this.handleFail);
  }

}
