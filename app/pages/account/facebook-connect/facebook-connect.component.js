System.register(['angular2/core', 'angular2/router', '../../../models/logs/log.service', '../../../utils/ajax.service', '../../../utils/session.service', '../../../utils/facebook.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, log_service_1, ajax_service_1, session_service_1, facebook_service_1;
    var FacebookConnectComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (log_service_1_1) {
                log_service_1 = log_service_1_1;
            },
            function (ajax_service_1_1) {
                ajax_service_1 = ajax_service_1_1;
            },
            function (session_service_1_1) {
                session_service_1 = session_service_1_1;
            },
            function (facebook_service_1_1) {
                facebook_service_1 = facebook_service_1_1;
            }],
        execute: function() {
            FacebookConnectComponent = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function FacebookConnectComponent(_ajaxService, log, _router, _session, _facebook) {
                    var _this = this;
                    this._ajaxService = _ajaxService;
                    this.log = log;
                    this._router = _router;
                    this._session = _session;
                    this._facebook = _facebook;
                    /*************************************
                     * PROPERTIES
                     *************************************/
                    this.showFacebookErrorMessage = false;
                    this.errors = {};
                    this.sending = false;
                    /*************************************
                     * METHODS
                     *************************************/
                    this.handleFail = function (err) {
                        _this.sending = false;
                        _this.log.error("Connect with Facebook Fail", err);
                        _this.showFacebookErrorMessage = true;
                    };
                    this.handleNewUser = function (response) {
                        localStorage.session = JSON.stringify(response);
                        _this.sending = false;
                        if (response["user_account_create"]) {
                            _this.log.account("Register", "With Facebook");
                            var params = { register: "true" };
                            _this.log.register(response.user_id);
                        }
                        else {
                            var params = { login: "true" };
                        }
                        _this.log.account("Login", "With Facebook");
                        _this.log.identify();
                        $('#session-check').trigger('click');
                        _this._router.navigate(['Home', params]);
                    };
                    this.sendToAPI = function (response) {
                        var options = {
                            data: JSON.stringify(response),
                            path: 'facebook-connect',
                            action: 'POST'
                        };
                        return _this._ajaxService.send(options);
                    };
                }
                /*************************************
                 * INITIALIZER
                 *************************************/
                FacebookConnectComponent.prototype.ngOnInit = function () {
                };
                FacebookConnectComponent.prototype.facebookLogin = function () {
                    var _this = this;
                    var log = this.log;
                    this.sending = true;
                    this.errors = {};
                    this.showErrorMessage = false;
                    this.showFacebookErrorMessage = false;
                    this._facebook.connect()
                        .then(this.sendToAPI)
                        .then(this.handleNewUser)
                        .catch(this.handleFail);
                };
                FacebookConnectComponent = __decorate([
                    core_1.Component({
                        selector: 'facebook-connect',
                        templateUrl: 'app/pages/account/facebook-connect/facebook-connect.component.html'
                    }), 
                    __metadata('design:paramtypes', [ajax_service_1.AjaxService, log_service_1.LogService, router_1.Router, session_service_1.SessionService, facebook_service_1.FacebookService])
                ], FacebookConnectComponent);
                return FacebookConnectComponent;
            })();
            exports_1("FacebookConnectComponent", FacebookConnectComponent);
        }
    }
});
//# sourceMappingURL=facebook-connect.component.js.map