System.register(['angular2/core', 'angular2/router', '../../form-errors/form-errors.component', '../login-links/login-links.component', '../../../utils/ajax.service', '../../../models/logs/log.service', '../../../utils/session.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, form_errors_component_1, login_links_component_1, ajax_service_1, log_service_1, session_service_1;
    var PasswordResetComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (form_errors_component_1_1) {
                form_errors_component_1 = form_errors_component_1_1;
            },
            function (login_links_component_1_1) {
                login_links_component_1 = login_links_component_1_1;
            },
            function (ajax_service_1_1) {
                ajax_service_1 = ajax_service_1_1;
            },
            function (log_service_1_1) {
                log_service_1 = log_service_1_1;
            },
            function (session_service_1_1) {
                session_service_1 = session_service_1_1;
            }],
        execute: function() {
            PasswordResetComponent = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function PasswordResetComponent(_ajaxService, _routeParams, _router, log, _session) {
                    this._ajaxService = _ajaxService;
                    this._routeParams = _routeParams;
                    this._router = _router;
                    this.log = log;
                    this._session = _session;
                    this.submitted = false;
                    this.linksPage = "passwordReset";
                    this.errors = {};
                    this.showErrorMessage = false;
                    this.sending = false;
                }
                /*************************************
                 * INITIALIZER
                 *************************************/
                PasswordResetComponent.prototype.ngOnInit = function () {
                    this.log.pageView("/password-reset");
                    this._session.redirectIfLoggedIn();
                    this.token = this._routeParams.get('token');
                    this.id = this._routeParams.get('id');
                };
                /*************************************
                 * METHODS
                 *************************************/
                PasswordResetComponent.prototype.validationsPass = function (source) {
                    if (this.newPassword.length < 8) {
                        var error = ["Password must have at least 8 characters."];
                        source.errors.newPassword = error;
                        this.log.error("Password Reset Submit", error);
                        this.sending = false;
                        return false;
                    }
                    if (this.newPassword !== this.newPasswordConfirmation) {
                        var error = ["Password and Password Confirmation do not match."];
                        source.errors.newPasswordConfirmation = error;
                        this.log.error("Password Reset Submit", error);
                        this.sending = false;
                        return false;
                    }
                    return true;
                };
                PasswordResetComponent.prototype.onSubmit = function () {
                    var _this = this;
                    var log = this.log;
                    log.formSubmit("Password Reset");
                    this.sending = true;
                    this.errors = {};
                    this.showErrorMessage = false;
                    if (!this.validationsPass(this))
                        return;
                    var options = {
                        data: JSON.stringify({
                            newPassword: this.newPassword,
                            token: this.token,
                            id: this.id
                        }),
                        path: 'password-reset',
                        action: 'PATCH'
                    };
                    _this._ajaxService.send(options)
                        .then(function () {
                        log.account("Reset", "Password");
                        _this.sending = false;
                        _this._router.navigate([
                            'Login', { passwordReset: 'true' }
                        ]);
                    })
                        .catch(function (err) {
                        _this.sending = false;
                        if (err &&
                            err.responseJSON &&
                            err.responseJSON.errors) {
                            log.error("Password Reset Submit", err.responseJSON.errors);
                            _this.errors.newPassword = [err.responseJSON.errors];
                        }
                        else {
                            log.error("Password Reset Submit", err);
                            _this.showErrorMessage = true;
                        }
                    });
                };
                PasswordResetComponent = __decorate([
                    core_1.Component({
                        selector: 'password-reset-request',
                        templateUrl: 'app/pages/account/password-reset/password-reset.component.html',
                        directives: [login_links_component_1.LoginLinksComponent, form_errors_component_1.FormErrorsComponent]
                    }), 
                    __metadata('design:paramtypes', [ajax_service_1.AjaxService, router_1.RouteParams, router_1.Router, log_service_1.LogService, session_service_1.SessionService])
                ], PasswordResetComponent);
                return PasswordResetComponent;
            })();
            exports_1("PasswordResetComponent", PasswordResetComponent);
        }
    }
});
//# sourceMappingURL=password-reset.component.js.map