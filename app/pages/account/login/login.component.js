System.register(['angular2/core', 'angular2/router', '../../form-errors/form-errors.component', '../login-links/login-links.component', '../facebook-connect/facebook-connect.component', '../../../models/logs/log.service', '../../../utils/ajax.service', '../../../utils/session.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, form_errors_component_1, login_links_component_1, facebook_connect_component_1, log_service_1, ajax_service_1, session_service_1;
    var LoginComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (form_errors_component_1_1) {
                form_errors_component_1 = form_errors_component_1_1;
            },
            function (login_links_component_1_1) {
                login_links_component_1 = login_links_component_1_1;
            },
            function (facebook_connect_component_1_1) {
                facebook_connect_component_1 = facebook_connect_component_1_1;
            },
            function (log_service_1_1) {
                log_service_1 = log_service_1_1;
            },
            function (ajax_service_1_1) {
                ajax_service_1 = ajax_service_1_1;
            },
            function (session_service_1_1) {
                session_service_1 = session_service_1_1;
            }],
        execute: function() {
            LoginComponent = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function LoginComponent(_routeParams, _ajaxService, log, _router, _session) {
                    this._routeParams = _routeParams;
                    this._ajaxService = _ajaxService;
                    this.log = log;
                    this._router = _router;
                    this._session = _session;
                    /*************************************
                     * PROPERTIES
                     *************************************/
                    this.confirmationSuccess = false;
                    this.passwordReset = false;
                    this.confirmationFail = false;
                    this.logoutSuccessful = false;
                    this.unauthorized = false;
                    this.errors = {};
                    this.submitted = false;
                    this.linksPage = 'login';
                    this.sending = false;
                }
                /*************************************
                 * INITIALIZER
                 *************************************/
                LoginComponent.prototype.ngOnInit = function () {
                    this.log.pageView("/login");
                    this._session.redirectIfLoggedIn();
                    this.checkAccountConfirmation();
                    this.checkPasswordReset();
                    this.checkLogout();
                    this.checkUnauthorized();
                };
                /*************************************
                 * METHODS
                 *************************************/
                LoginComponent.prototype.onSubmit = function () {
                    var _this = this;
                    var log = this.log;
                    log.formSubmit("Login");
                    this.sending = true;
                    this.errors = {};
                    this.showErrorMessage = false;
                    if (this.password.length < 8) {
                        var error = ["Password must be at least 8 characters"];
                        this.sending = false;
                        log.error("Login with Email and Password Fail", error);
                        return this.errors.password = error;
                    }
                    var options = {
                        data: JSON.stringify({
                            email: this.email,
                            password: this.password
                        }),
                        path: 'sessions',
                        action: 'POST'
                    };
                    _this._ajaxService.send(options)
                        .then(function (response) {
                        localStorage.session = JSON.stringify(response);
                        _this.sending = false;
                        _this.submitted = true;
                        log.account("Login", "With Email and Password");
                        _this.log.identify();
                        $('#session-check').trigger('click');
                        _this._router.navigate(['Home', { login: "true" }]);
                    })
                        .catch(function (err) {
                        _this.sending = false;
                        if (err &&
                            err.responseJSON &&
                            err.responseJSON.errors) {
                            _this.errors.email = [err.responseJSON.errors];
                            log.error("Login with Email and Password Fail", err.responseJSON.errors);
                        }
                        else {
                            log.error("Login with Email and Password Fail", err);
                            _this.showErrorMessage = true;
                        }
                    });
                };
                LoginComponent.prototype.checkAccountConfirmation = function () {
                    var confirmed = this._routeParams.get('confirmed');
                    if (confirmed && confirmed === "true") {
                        this.log.account("Confirm Account", "through email");
                        this.confirmationSuccess = true;
                        this.confirmationFail = false;
                    }
                    else if (confirmed && confirmed === "false") {
                        this.log.error("Account Confirm", "Fail");
                        this.confirmationSuccess = false;
                        this.confirmationFail = true;
                    }
                };
                LoginComponent.prototype.checkPasswordReset = function () {
                    var passwordReset = this._routeParams.get('passwordReset');
                    if (passwordReset) {
                        this.passwordReset = true;
                    }
                };
                LoginComponent.prototype.checkLogout = function () {
                    var _this = this;
                    var logout = this._routeParams.get('logout');
                    if (logout && logout === "true") {
                        this.logoutSuccess = true;
                        setTimeout(function () { _this.logoutSuccess = false; }, 5000);
                    }
                };
                LoginComponent.prototype.checkUnauthorized = function () {
                    var _this = this;
                    if (this._routeParams.get('unauthorized')) {
                        this.unauthorized = true;
                        setTimeout(function () { _this.unauthorized = false; }, 5000);
                    }
                };
                LoginComponent = __decorate([
                    core_1.Component({
                        selector: 'login-form',
                        templateUrl: 'app/pages/account/login/login.component.html',
                        directives: [
                            login_links_component_1.LoginLinksComponent,
                            form_errors_component_1.FormErrorsComponent,
                            facebook_connect_component_1.FacebookConnectComponent
                        ]
                    }), 
                    __metadata('design:paramtypes', [router_1.RouteParams, ajax_service_1.AjaxService, log_service_1.LogService, router_1.Router, session_service_1.SessionService])
                ], LoginComponent);
                return LoginComponent;
            })();
            exports_1("LoginComponent", LoginComponent);
        }
    }
});
//# sourceMappingURL=login.component.js.map