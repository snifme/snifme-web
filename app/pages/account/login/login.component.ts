import {Component,OnInit} from 'angular2/core';
import {RouteParams,Router} from 'angular2/router';
import {NgForm}           from 'angular2/common';
import {User}             from '../../../models/users/user';
import {FormErrorsComponent} from '../../form-errors/form-errors.component';
import {LoginLinksComponent} from '../login-links/login-links.component';
import {FacebookConnectComponent} from '../facebook-connect/facebook-connect.component';
import {LogService} from '../../../models/logs/log.service';
import {AjaxService} from '../../../utils/ajax.service';
import {SessionService} from '../../../utils/session.service';
import {FacebookService} from '../../../utils/facebook.service';

@Component({
  selector: 'login-form',
  templateUrl: 'app/pages/account/login/login.component.html',
  directives: [
    LoginLinksComponent,
    FormErrorsComponent,
    FacebookConnectComponent
  ]
})

export class LoginComponent implements OnInit {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private _routeParams:RouteParams,
    private _ajaxService:AjaxService,
    private log: LogService,
    private _router: Router,
    private _session: SessionService
  ){}

  /*************************************
   * PROPERTIES
   *************************************/
  confirmationSuccess: boolean = false;
  passwordReset: boolean = false;
  confirmationFail: boolean = false;
  logoutSuccessful: boolean = false;
  unauthorized: boolean = false;
  email:string;
  password:string;
  errors = {};
  submitted = false;
  linksPage: string = 'login';
  sending: boolean = false;

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
    this.log.pageView("/login");
    this._session.redirectIfLoggedIn();
    this.checkAccountConfirmation();
    this.checkPasswordReset();
    this.checkLogout();
    this.checkUnauthorized();
  }

  /*************************************
   * METHODS
   *************************************/

  onSubmit() {
    const _this = this;
    const log = this.log;
    log.formSubmit("Login");
    this.sending = true;
    this.errors = {};
    this.showErrorMessage = false;

    if (this.password.length < 8) {
      const error = ["Password must be at least 8 characters"];
      this.sending = false;
      log.error("Login with Email and Password Fail",error);
      return this.errors.password = error;
    }

    const options = {
      data: JSON.stringify({
        email: this.email,
        password: this.password
      }),
      path: 'sessions',
      action: 'POST'
    };

    _this._ajaxService.send(options)
    .then((response) => {
      localStorage.session = JSON.stringify(response);
      _this.sending = false;
      _this.submitted = true;
      log.account("Login", "With Email and Password");
      _this.log.identify();
      $('#session-check').trigger('click');
      _this._router.navigate(['Home', {login: "true"}]);
    })
    .catch((err) => {
      _this.sending = false;
      if (err &&
          err.responseJSON &&
          err.responseJSON.errors) {
        _this.errors.email = [err.responseJSON.errors];
        log.error("Login with Email and Password Fail", err.responseJSON.errors);
      } else {
        log.error("Login with Email and Password Fail", err);
        _this.showErrorMessage = true;
      }

    });
  }

  checkAccountConfirmation() {
    let confirmed = this._routeParams.get('confirmed');
    if (confirmed && confirmed === "true") {
      this.log.account("Confirm Account", "through email");
      this.confirmationSuccess = true;
      this.confirmationFail = false;
    } else if (confirmed && confirmed === "false") {
      this.log.error("Account Confirm", "Fail");
      this.confirmationSuccess = false;
      this.confirmationFail = true;
    }
  }

  checkPasswordReset() {
    let passwordReset = this._routeParams.get('passwordReset');
    if (passwordReset) {
      this.passwordReset = true;
    }
  }

  checkLogout() {
    const _this = this;
    let logout = this._routeParams.get('logout');
    if (logout && logout === "true") {
      this.logoutSuccess = true;
      setTimeout(() => { _this.logoutSuccess = false; }, 5000);
    }
  }

  checkUnauthorized() {
    const _this = this;
    if (this._routeParams.get('unauthorized')) {
      this.unauthorized = true;
      setTimeout(() => { _this.unauthorized = false; }, 5000);
    }
  }

}
