import {Component} from 'angular2/core';

@Component({
  selector: 'form-errors',
  templateUrl: 'app/pages/form-errors/form-errors.component.html',
  inputs: ['errors']
})

export class FormErrorsComponent {

  // properties
  errors: string[];
}
