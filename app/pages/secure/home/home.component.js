System.register(['angular2/core', 'angular2/router', '../../../models/logs/log.service', '../../../utils/session.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, log_service_1, session_service_1;
    var HomeComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (log_service_1_1) {
                log_service_1 = log_service_1_1;
            },
            function (session_service_1_1) {
                session_service_1 = session_service_1_1;
            }],
        execute: function() {
            HomeComponent = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function HomeComponent(log, _routeParams, _session, _router) {
                    this.log = log;
                    this._routeParams = _routeParams;
                    this._session = _session;
                    this._router = _router;
                    /*************************************
                     * PROPERTIES
                     *************************************/
                    this.loginSuccess = false;
                    this.registrationSuccess = false;
                }
                /*************************************
                 * INITIALIZER
                 *************************************/
                HomeComponent.prototype.ngOnInit = function () {
                    if (window.loggingOut !== true) {
                        this.log.pageView("/home");
                        this._session.redirectIfNotLoggedIn();
                        this.addFlashIfParam('register', 'registrationSuccess');
                        this.addFlashIfParam('login', 'loginSuccess');
                    }
                    else {
                        window.loggingOut = false;
                        this._router.navigate(["Login", { logout: "true" }]);
                    }
                };
                HomeComponent.prototype.addFlashIfParam = function (param, flashProperty) {
                    var _this = this;
                    var property = this._routeParams.get(param);
                    if (property && property === "true" || property === true) {
                        this[flashProperty] = true;
                        setTimeout(function () { _this[flashProperty] = false; }, 5000);
                    }
                };
                HomeComponent = __decorate([
                    core_1.Component({
                        selector: 'user-home',
                        templateUrl: 'app/pages/secure/home/home.component.html',
                        directives: [router_1.ROUTER_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [log_service_1.LogService, router_1.RouteParams, session_service_1.SessionService, router_1.Router])
                ], HomeComponent);
                return HomeComponent;
            })();
            exports_1("HomeComponent", HomeComponent);
        }
    }
});
//# sourceMappingURL=home.component.js.map