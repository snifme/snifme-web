import {Component,OnInit} from 'angular2/core';
import {RouteParams,Router,ROUTER_DIRECTIVES} from 'angular2/router';
import {LogService} from '../../../models/logs/log.service';
import {SessionService} from '../../../utils/session.service';

@Component({
  selector: 'user-home',
  templateUrl: 'app/pages/secure/home/home.component.html',
  directives: [ROUTER_DIRECTIVES]
})

export class HomeComponent implements OnInit {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private log: LogService,
    private _routeParams: RouteParams,
    private _session: SessionService,
    private _router: Router
  ) {}

  /*************************************
   * PROPERTIES
   *************************************/
  loginSuccess = false;
  registrationSuccess = false;

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
    if (window.loggingOut !== true) {
      this.log.pageView("/home");
      this._session.redirectIfNotLoggedIn();
      this.addFlashIfParam('register', 'registrationSuccess');
      this.addFlashIfParam('login', 'loginSuccess');
    } else {
      window.loggingOut = false;
      this._router.navigate(["Login", {logout: "true"}]);
    }
  }

  addFlashIfParam(param, flashProperty) {
    const _this = this;
    let property = this._routeParams.get(param);
    if (property && property === "true" || property === true) {
      this[flashProperty] = true;
      setTimeout(() => { _this[flashProperty] = false; }, 5000);
    }
  }

}
