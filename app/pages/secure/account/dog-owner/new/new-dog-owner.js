System.register(['angular2/core', 'angular2/router', '../../../../form-errors/form-errors.component', '../../../../../components/forms/address/address', '../../../../../utils/ajax.service', '../../../../../models/logs/log.service', '../../../../../utils/session.service', '../../../../../utils/geocode.service', '../../../../../utils/current-user.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, form_errors_component_1, address_1, ajax_service_1, log_service_1, session_service_1, geocode_service_1, current_user_service_1;
    var NewDogOwnerPage;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (form_errors_component_1_1) {
                form_errors_component_1 = form_errors_component_1_1;
            },
            function (address_1_1) {
                address_1 = address_1_1;
            },
            function (ajax_service_1_1) {
                ajax_service_1 = ajax_service_1_1;
            },
            function (log_service_1_1) {
                log_service_1 = log_service_1_1;
            },
            function (session_service_1_1) {
                session_service_1 = session_service_1_1;
            },
            function (geocode_service_1_1) {
                geocode_service_1 = geocode_service_1_1;
            },
            function (current_user_service_1_1) {
                current_user_service_1 = current_user_service_1_1;
            }],
        execute: function() {
            NewDogOwnerPage = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function NewDogOwnerPage(log, _ajaxService, _router, _session, geocode, currentUserService) {
                    var _this = this;
                    this.log = log;
                    this._ajaxService = _ajaxService;
                    this._router = _router;
                    this._session = _session;
                    this.geocode = geocode;
                    this.currentUserService = currentUserService;
                    /*************************************
                     * PROPERTIES
                     *************************************/
                    this.firstName = "Monty"; //null;
                    this.lastName = "Lennie"; //null;
                    this.street1 = "1010 Howe street"; //null;
                    this.street2 = null;
                    this.city = "Vancouver"; //null;
                    this.state = "BC"; //null;
                    this.country = "Canada"; //null;
                    this.zipcode = "v6z 1p5"; //null;
                    this.zipcodePlus = null;
                    this.geocodedResults = {};
                    this.currentUser = {};
                    this.hasName = false;
                    this.showErrorMessage = false;
                    this.showValidationError = false;
                    this.sending = false;
                    this.errors = {};
                    this.submitted = false;
                    /*************************************
                     * METHODS
                     *************************************/
                    this.setCurrentUser = function () {
                        _this.currentUser = _this.currentUserService.getInfo();
                    };
                    this.checkIfHasName = function () {
                        if (_this.currentUser &&
                            _this.currentUser.firstName &&
                            _this.currentUser.lastName) {
                            _this.hasName = true;
                        }
                        else {
                            _this.hasName = false;
                        }
                    };
                    this.validateData = function () {
                        var hasErrors = false;
                        var self = _this;
                        var fields = [
                            ["street1", _this.street1, "required"],
                            ["city", _this.city, "required"],
                            ["state", _this.state, "required"],
                            ["country", _this.country, "required"],
                            ["zipcode", _this.zipcode, "required"]
                        ];
                        if (!_this.hasName) {
                            fields.push(["First Name", _this.firstName, "required"], ["Last Name", _this.lastName, "required"]);
                        }
                        fields.forEach(function (field) {
                            if (field[2] === "required" && !field[1]) {
                                self.errors[field[0]] = "Is required";
                                hasErrors = true;
                            }
                        });
                        if (hasErrors) {
                            throw new Error({ validationError: "Please fill in all required fields" });
                        }
                        else {
                            return true;
                        }
                    };
                    this.prepareData = function () {
                        var geo = _this.geocodedResults;
                        var data = {
                            "account": {
                                "kind": "dog_owner",
                                "addresses_attributes": [{
                                        "street1": _this.street1,
                                        "street2": _this.street2,
                                        "city": _this.city,
                                        "state": _this.state,
                                        "country": _this.country,
                                        "zipcode": _this.zipcode,
                                        "zipcode_extra": _this.zipcodePlus,
                                        "latitude": geo.latitude,
                                        "longitude": geo.longitude,
                                        "geocoded_number": geo.geocodedNumber,
                                        "geocoded_street": geo.geocodedStreet,
                                        "geocoded_city": geo.geocodedCity,
                                        "geocoded_state": geo.geocodedState,
                                        "geocoded_state_short": geo.geocodedStateShort,
                                        "geocoded_country": geo.geocodedCountry,
                                        "geocoded_zipcode": geo.geocodedZipcode,
                                        "geocoded_country_short": geo.geocodedCountryShort
                                    }]
                            }
                        };
                        if (!_this.hasName) {
                            data.user = {
                                "first_name": _this.firstName,
                                "last_name": _this.lastName
                            };
                        }
                        return JSON.stringify(data);
                    };
                    this.geocodeAddress = function () {
                        var self = _this;
                        var address = (_this.street1 + " " + (_this.street2 || "") + " " + _this.city + " ") +
                            (" " + _this.state + " " + _this.country + " " + _this.zipcode);
                        return _this.geocode.geocode(address)
                            .then(function (results) {
                            self.geocodedResults = results;
                            return true;
                        });
                    };
                    this.prepareAjax = function (data) {
                        return {
                            data: data,
                            path: 'accounts',
                            action: 'POST'
                        };
                    };
                    this.handleError = function (err) {
                        _this.sending = false;
                        if (err &&
                            err.responseJSON &&
                            err.responseJSON.errors) {
                            log.error("Create Dog Owner", err.responseJSON.errors);
                            _this.errors.email = [err.responseJSON.errors];
                        }
                        else if (err && err.validationError) {
                            log.error("Create Dog Owner", err);
                            _this.showErrorMessage = true;
                            _this.errorMessage = "Form Validation Errors. Please fill in form as requested";
                        }
                        else if (err && err.geocodeError && err.geocodeError === "ZERO_RESULTS") {
                            log.error("Create Dog Owner", err);
                            console.log(err);
                            _this.showErrorMessage = true;
                            _this.errorMessage = "Address could not be found. Please enter valid address";
                        }
                        else {
                            log.error("Create Dog Owner", err);
                            console.log(err);
                            _this.showErrorMessage = true;
                            _this.errorMessage = "There was an error and the dog owner account could not be created.\n                           Please try again soon";
                        }
                    };
                    this.startFormSubmission = function () {
                        var self = _this;
                        var log = _this.log;
                        log.formSubmit("Create Dog Owner");
                        _this.sending = true;
                        _this.errors = {};
                        _this.showErrorMessage = false;
                        return "done";
                    };
                    this.handleSuccess = function (res) {
                        _this.sending = false;
                        debugger;
                    };
                }
                /*************************************
                 * INITIALIZER
                 *************************************/
                NewDogOwnerPage.prototype.ngOnInit = function () {
                    if (window.loggingOut !== true) {
                        this.log.pageView("/new-dog-owner");
                        this._session.redirectIfNotLoggedIn();
                    }
                    else {
                        window.loggingOut = false;
                        this._router.navigate(["Login", { logout: "true" }]);
                    }
                    this.setCurrentUser();
                    this.checkIfHasName();
                };
                NewDogOwnerPage.prototype.onSubmit = function () {
                    var self = this;
                    Promise.resolve()
                        .then(self.startFormSubmission)
                        .then(self.validateData)
                        .then(self.geocodeAddress)
                        .then(self.prepareData)
                        .then(self.prepareAjax)
                        .then(self.sendAjax)
                        .then(function (options) {
                        return self._ajaxService.send(options);
                    })
                        .then(self.handleSuccess)
                        .catch(self.handleError);
                };
                NewDogOwnerPage = __decorate([
                    core_1.Component({
                        templateUrl: 'app/pages/secure/account/dog-owner/new/new-dog-owner.html',
                        directives: [
                            router_1.ROUTER_DIRECTIVES,
                            form_errors_component_1.FormErrorsComponent,
                            address_1.AddressFieldsComponent
                        ]
                    }), 
                    __metadata('design:paramtypes', [log_service_1.LogService, ajax_service_1.AjaxService, router_1.Router, session_service_1.SessionService, geocode_service_1.GeocodeService, current_user_service_1.CurrentUserService])
                ], NewDogOwnerPage);
                return NewDogOwnerPage;
            })();
            exports_1("NewDogOwnerPage", NewDogOwnerPage);
        }
    }
});
//# sourceMappingURL=new-dog-owner.js.map