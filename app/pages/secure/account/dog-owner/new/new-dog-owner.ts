import {Component,OnInit} from 'angular2/core';
import {Router,ROUTER_DIRECTIVES} from 'angular2/router';
import {FormErrorsComponent} from '../../../../form-errors/form-errors.component';
import {AddressFieldsComponent} from '../../../../../components/forms/address/address';
import {AjaxService} from '../../../../../utils/ajax.service';
import {LogService} from '../../../../../models/logs/log.service';
import {SessionService} from '../../../../../utils/session.service';
import {GeocodeService} from '../../../../../utils/geocode.service';
import {CurrentUserService} from '../../../../../utils/current-user.service';

@Component({
  templateUrl: 'app/pages/secure/account/dog-owner/new/new-dog-owner.html',
  directives: [
    ROUTER_DIRECTIVES,
    FormErrorsComponent,
    AddressFieldsComponent
  ]
})

export class NewDogOwnerPage implements OnInit {

  /*************************************
   * PROPERTIES
   *************************************/
  firstName:string = "Monty";//null;
  lastName:string = "Lennie";//null;
  street1:string = "1010 Howe street";//null;
  street2:string = null;
  city:string = "Vancouver";//null;
  state:string = "BC"//null;
  country:string = "Canada"//null;
  zipcode:string = "v6z 1p5"//null;
  zipcodePlus:string = null;
  geocodedResults:Object = {};
  currentUser:Object = {};
  hasName:boolean = false;

  showErrorMessage: boolean = false;
  errorMessage:string;
  showValidationError: boolean = false;
  sending:boolean = false;
  errors = {};
  submitted = false;

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private log: LogService,
    private _ajaxService:AjaxService,
    private _router: Router,
    private _session: SessionService,
    private geocode: GeocodeService,
    private currentUserService: CurrentUserService
  ) {}

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
    if (window.loggingOut !== true) {
      this.log.pageView("/new-dog-owner");
      this._session.redirectIfNotLoggedIn();
    } else {
      window.loggingOut = false;
      this._router.navigate(["Login", {logout: "true"}]);
    }
    this.setCurrentUser();
    this.checkIfHasName();
  }

  /*************************************
   * METHODS
   *************************************/
  private setCurrentUser = () => {
    this.currentUser = this.currentUserService.getInfo();
  }

  private checkIfHasName = () => {
    if (this.currentUser &&
        this.currentUser.firstName &&
        this.currentUser.lastName) {
      this.hasName = true;
    } else {
      this.hasName = false;
    }

  }

  private validateData = () => {
    let hasErrors = false;
    const self = this;

    let fields = [
      ["street1", this.street1, "required"],
      ["city", this.city, "required"],
      ["state", this.state, "required"],
      ["country", this.country, "required"],
      ["zipcode", this.zipcode, "required"]
    ];

    if (!this.hasName) {
      fields.push(
        ["First Name", this.firstName, "required"],
        ["Last Name", this.lastName, "required"]
      )
    }

    fields.forEach((field) => {
      if (field[2] === "required" && !field[1]) {
        self.errors[field[0]] = "Is required";
        hasErrors = true;
      }
    })

    if (hasErrors) {
      throw new Error({validationError: "Please fill in all required fields");
    } else {
      return true;
    }
  }

  private prepareData = () => {
    const geo = this.geocodedResults;
    let data = {
      "account": {
        "kind": "dog_owner"
        "addresses_attributes": [{
          "street1": this.street1,
          "street2": this.street2,
          "city": this.city,
          "state": this.state,
          "country": this.country,
          "zipcode": this.zipcode,
          "zipcode_extra": this.zipcodePlus,
          "latitude": geo.latitude,
          "longitude": geo.longitude,
          "geocoded_number": geo.geocodedNumber,
          "geocoded_street": geo.geocodedStreet,
          "geocoded_city": geo.geocodedCity,
          "geocoded_state": geo.geocodedState,
          "geocoded_state_short": geo.geocodedStateShort,
          "geocoded_country": geo.geocodedCountry,
          "geocoded_zipcode": geo.geocodedZipcode,
          "geocoded_country_short": geo.geocodedCountryShort
        }]
      }
    };

    if (!this.hasName) {
      data.user = {
        "first_name": this.firstName,
        "last_name": this.lastName
      }
    }

    return JSON.stringify(data);
  }

  private geocodeAddress = () => {
    const self = this;
    const address = `${this.street1} ${this.street2 || ""} ${this.city} ` +
                    ` ${this.state} ${this.country} ${this.zipcode}`;
    return this.geocode.geocode(address)
      .then((results) => {
        self.geocodedResults = results;
        return true;
      });
  }

  private prepareAjax = (data) => {
    return {
      data: data,
      path: 'accounts',
      action: 'POST'
    };
  }

  private handleError = (err) => {
    this.sending = false;
    if (err &&
        err.responseJSON &&
        err.responseJSON.errors) {
        log.error("Create Dog Owner", err.responseJSON.errors);
        _this.errors.email = [err.responseJSON.errors];
    } else if (err && err.validationError) {
        log.error("Create Dog Owner",err);
      this.showErrorMessage = true;
      this.errorMessage = "Form Validation Errors. Please fill in form as requested";
    } else if (err && err.geocodeError && err.geocodeError === "ZERO_RESULTS") {
        log.error("Create Dog Owner",err);
        console.log(err);
      this.showErrorMessage = true;
      this.errorMessage = `Address could not be found. Please enter valid address`;
    } else {
        log.error("Create Dog Owner",err);
        console.log(err);
      this.showErrorMessage = true;
      this.errorMessage = `There was an error and the dog owner account could not be created.
                           Please try again soon`;
    }
  }

  private startFormSubmission = () => {
    const self = this;
    const log = this.log;
    log.formSubmit("Create Dog Owner");
    this.sending = true;
    this.errors = {};
    this.showErrorMessage = false;
    return "done";
  }

  private handleSuccess = (res) {
    this.sending = false;
    debugger;
  }

  onSubmit() {
    const self = this;
    Promise.resolve()
    .then(self.startFormSubmission)
    .then(self.validateData)
    .then(self.geocodeAddress)
    .then(self.prepareData)
    .then(self.prepareAjax)
    .then(self.sendAjax)
    .then((options) => {
      return self._ajaxService.send(options);
    })
    .then(self.handleSuccess)
    .catch(self.handleError);
  }

}
