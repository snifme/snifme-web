System.register(['angular2/core', '../../../pages/form-errors/form-errors.component'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, form_errors_component_1;
    var AddressFieldsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (form_errors_component_1_1) {
                form_errors_component_1 = form_errors_component_1_1;
            }],
        execute: function() {
            AddressFieldsComponent = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function AddressFieldsComponent() {
                    /*************************************
                     * METHODS
                     *************************************/
                    this.countries = "\n    Afghanistan\n    Albania\n    Algeria\n    Andorra\n    Angola\n    Antigua and Barbuda\n    Argentina\n    Armenia\n    Aruba\n    Australia\n    Austria\n    Azerbaijan\n    Bahamas, The\n    Bahrain\n    Bangladesh\n    Barbados\n    Belarus\n    Belgium\n    Belize\n    Benin\n    Bhutan\n    Bolivia\n    Bosnia and Herzegovina\n    Botswana\n    Brazil\n    Brunei\n    Bulgaria\n    Burkina Faso\n    Burma\n    Burundi\n    Cambodia\n    Cameroon\n    Canada\n    Cape Verde\n    Central African Republic\n    Chad\n    Chile\n    China\n    Colombia\n    Comoros\n    Congo, Democratic Republic of the\n    Congo, Republic of the\n    Costa Rica\n    Cote d'Ivoire\n    Croatia\n    Cuba\n    Curacao\n    Cyprus\n    Czech Republic\n    Denmark\n    Djibouti\n    Dominica\n    Dominican Republic\n    East Timor (see Timor-Leste)\n    Ecuador\n    Egypt\n    El Salvador\n    Equatorial Guinea\n    Eritrea\n    Estonia\n    Ethiopia\n    Fiji\n    Finland\n    France\n    Gabon\n    Gambia, The\n    Georgia\n    Germany\n    Ghana\n    Greece\n    Grenada\n    Guatemala\n    Guinea\n    Guinea-Bissau\n    Guyana\n    Haiti\n    Holy See\n    Honduras\n    Hong Kong\n    Hungary\n    Iceland\n    India\n    Indonesia\n    Iran\n    Iraq\n    Ireland\n    Israel\n    Italy\n    Jamaica\n    Japan\n    Jordan\n    Kazakhstan\n    Kenya\n    Kiribati\n    Korea, North\n    Korea, South\n    Kosovo\n    Kuwait\n    Kyrgyzstan\n    Laos\n    Latvia\n    Lebanon\n    Lesotho\n    Liberia\n    Libya\n    Liechtenstein\n    Lithuania\n    Luxembourg\n    Macau\n    Macedonia\n    Madagascar\n    Malawi\n    Malaysia\n    Maldives\n    Mali\n    Malta\n    Marshall Islands\n    Mauritania\n    Mauritius\n    Mexico\n    Micronesia\n    Moldova\n    Monaco\n    Mongolia\n    Montenegro\n    Morocco\n    Mozambique\n    Namibia\n    Nauru\n    Nepal\n    Netherlands\n    Netherlands Antilles\n    New Zealand\n    Nicaragua\n    Niger\n    Nigeria\n    North Korea\n    Norway\n    Oman\n    Pakistan\n    Palau\n    Palestinian Territories\n    Panama\n    Papua New Guinea\n    Paraguay\n    Peru\n    Philippines\n    Poland\n    Portugal\n    Qatar\n    Romania\n    Russia\n    Rwanda\n    Saint Kitts and Nevis\n    Saint Lucia\n    Saint Vincent and the Grenadines\n    Samoa\n    San Marino\n    Sao Tome and Principe\n    Saudi Arabia\n    Senegal\n    Serbia\n    Seychelles\n    Sierra Leone\n    Singapore\n    Sint Maarten\n    Slovakia\n    Slovenia\n    Solomon Islands\n    Somalia\n    South Africa\n    South Korea\n    South Sudan\n    Spain\n    Sri Lanka\n    Sudan\n    Suriname\n    Swaziland\n    Sweden\n    Switzerland\n    Syria\n    Taiwan\n    Tajikistan\n    Tanzania\n    Thailand\n    Timor-Leste\n    Togo\n    Tonga\n    Trinidad and Tobago\n    Tunisia\n    Turkey\n    Turkmenistan\n    Tuvalu\n    Uganda\n    Ukraine\n    United Arab Emirates\n    United Kingdom\n    Uruguay\n    Uzbekistan\n    Vanuatu\n    Venezuela\n    Vietnam\n    Yemen\n    Zambia\n    Zimbabwe\n  ";
                    this.updateAddress = new core_1.EventEmitter;
                }
                /*************************************
                 * INITIALIZER
                 *************************************/
                AddressFieldsComponent.prototype.ngOnInit = function () {
                };
                AddressFieldsComponent = __decorate([
                    core_1.Component({
                        selector: 'address-fields',
                        templateUrl: 'app/components/forms/address/address.html',
                        directives: [
                            form_errors_component_1.FormErrorsComponent
                        ],
                        inputs: ['street1', 'street2', 'city', 'state', 'country', 'zipcode', 'zipcodePlus',
                            'errors'],
                        outputs: ['updateAddress']
                    }), 
                    __metadata('design:paramtypes', [])
                ], AddressFieldsComponent);
                return AddressFieldsComponent;
            })();
            exports_1("AddressFieldsComponent", AddressFieldsComponent);
        }
    }
});
//# sourceMappingURL=address.js.map