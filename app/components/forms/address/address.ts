import {Component,OnInit,EventEmitter} from 'angular2/core';
import {FormErrorsComponent} from '../../../pages/form-errors/form-errors.component';

@Component({
  selector: 'address-fields',
  templateUrl: 'app/components/forms/address/address.html',
  directives: [
    FormErrorsComponent
  ]
  inputs: ['street1','street2','city','state','country','zipcode','zipcodePlus',
           'errors'],
  outputs: ['updateAddress']
})

export class AddressFieldsComponent implements OnInit {

  /*************************************
   * PROPERTIES
   *************************************/
  updateAddress: EventEmitter;
  street1:string;
  street2:string;
  city:string;
  state:string;
  country:string;
  zipcode:string;
  zipcodePlus:string;

  errors;

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
   ) {
    this.updateAddress = new EventEmitter;
  }

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
  }

  /*************************************
   * METHODS
   *************************************/
  countries: `
    Afghanistan
    Albania
    Algeria
    Andorra
    Angola
    Antigua and Barbuda
    Argentina
    Armenia
    Aruba
    Australia
    Austria
    Azerbaijan
    Bahamas, The
    Bahrain
    Bangladesh
    Barbados
    Belarus
    Belgium
    Belize
    Benin
    Bhutan
    Bolivia
    Bosnia and Herzegovina
    Botswana
    Brazil
    Brunei
    Bulgaria
    Burkina Faso
    Burma
    Burundi
    Cambodia
    Cameroon
    Canada
    Cape Verde
    Central African Republic
    Chad
    Chile
    China
    Colombia
    Comoros
    Congo, Democratic Republic of the
    Congo, Republic of the
    Costa Rica
    Cote d'Ivoire
    Croatia
    Cuba
    Curacao
    Cyprus
    Czech Republic
    Denmark
    Djibouti
    Dominica
    Dominican Republic
    East Timor (see Timor-Leste)
    Ecuador
    Egypt
    El Salvador
    Equatorial Guinea
    Eritrea
    Estonia
    Ethiopia
    Fiji
    Finland
    France
    Gabon
    Gambia, The
    Georgia
    Germany
    Ghana
    Greece
    Grenada
    Guatemala
    Guinea
    Guinea-Bissau
    Guyana
    Haiti
    Holy See
    Honduras
    Hong Kong
    Hungary
    Iceland
    India
    Indonesia
    Iran
    Iraq
    Ireland
    Israel
    Italy
    Jamaica
    Japan
    Jordan
    Kazakhstan
    Kenya
    Kiribati
    Korea, North
    Korea, South
    Kosovo
    Kuwait
    Kyrgyzstan
    Laos
    Latvia
    Lebanon
    Lesotho
    Liberia
    Libya
    Liechtenstein
    Lithuania
    Luxembourg
    Macau
    Macedonia
    Madagascar
    Malawi
    Malaysia
    Maldives
    Mali
    Malta
    Marshall Islands
    Mauritania
    Mauritius
    Mexico
    Micronesia
    Moldova
    Monaco
    Mongolia
    Montenegro
    Morocco
    Mozambique
    Namibia
    Nauru
    Nepal
    Netherlands
    Netherlands Antilles
    New Zealand
    Nicaragua
    Niger
    Nigeria
    North Korea
    Norway
    Oman
    Pakistan
    Palau
    Palestinian Territories
    Panama
    Papua New Guinea
    Paraguay
    Peru
    Philippines
    Poland
    Portugal
    Qatar
    Romania
    Russia
    Rwanda
    Saint Kitts and Nevis
    Saint Lucia
    Saint Vincent and the Grenadines
    Samoa
    San Marino
    Sao Tome and Principe
    Saudi Arabia
    Senegal
    Serbia
    Seychelles
    Sierra Leone
    Singapore
    Sint Maarten
    Slovakia
    Slovenia
    Solomon Islands
    Somalia
    South Africa
    South Korea
    South Sudan
    Spain
    Sri Lanka
    Sudan
    Suriname
    Swaziland
    Sweden
    Switzerland
    Syria
    Taiwan
    Tajikistan
    Tanzania
    Thailand
    Timor-Leste
    Togo
    Tonga
    Trinidad and Tobago
    Tunisia
    Turkey
    Turkmenistan
    Tuvalu
    Uganda
    Ukraine
    United Arab Emirates
    United Kingdom
    Uruguay
    Uzbekistan
    Vanuatu
    Venezuela
    Vietnam
    Yemen
    Zambia
    Zimbabwe
  `

}
