System.register(['angular2/core', '../models/logs/log', '../config/config.service', './ajax.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, log_1, config_service_1, ajax_service_1;
    var LogService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (log_1_1) {
                log_1 = log_1_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (ajax_service_1_1) {
                ajax_service_1 = ajax_service_1_1;
            }],
        execute: function() {
            LogService = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function LogService(_config, _ajaxService) {
                    var _this = this;
                    this._config = _config;
                    this._ajaxService = _ajaxService;
                    /*************************************
                     * ERROR
                     *************************************/
                    this.error = function (action, label) {
                        if (label === void 0) { label = ""; }
                        if (typeof label !== "string") {
                            label = JSON.stringify(label);
                        }
                        // send event track
                        _this.track("Error", action, label);
                        // GOOGLE
                        ga('send', 'exception', {
                            'exDescription': action + " " + label,
                            'exFatal': false
                        });
                    };
                    this.setSessionToken = function (response) {
                        localStorage.sessionToken = response.token;
                    };
                    this.handleSessionTokenSetFailure = function (err) {
                        console.log(err);
                        _this.error("set", "session token");
                    };
                    this.removeSessionToken = function (response) {
                        localStorage.sessionToken = null;
                    };
                    this.handleSessionTokenMergeFailure = function (err) {
                        console.log(err);
                        _this.error("merge", "session token");
                    };
                    this.mergeSessionTokenLogsWithUser = function () {
                        var userId = _this._config.getUserId();
                        var sessionToken = _this._config.getSessionToken();
                        _this._ajaxService.send({
                            path: "/merge-log-session-token",
                            action: "PATCH",
                            data: JSON.stringify({
                                "session_token": sessionToken,
                                "user_id": userId
                            })
                        })
                            .then(_this.removeSessionToken)
                            .catch(_this.handleSessionTokenMergeFailure);
                    };
                }
                /*************************************
                 * GENERAL EVENT TRACK
                 *************************************/
                LogService.prototype.track = function (category, action, label, properties) {
                    if (label === void 0) { label = ""; }
                    if (properties === void 0) { properties = {}; }
                    // GOOGLE
                    ga('send', {
                        hitType: 'event',
                        eventCategory: category,
                        eventAction: action,
                        eventLabel: label
                    });
                    // CUSTOM
                    var data = {
                        category: category,
                        action: action,
                        label: label
                    };
                    this.createLog(data);
                    // MIXPANEL
                    if (category == "Error") {
                        mixpanel.track("Error", {
                            "Error Category": action,
                            "Error Info": label
                        });
                    }
                    else {
                        var name_1 = category + " " + action + " " + label;
                        mixpanel.track(name_1, properties);
                    }
                };
                /*************************************
                 * CUSTOM EVENTS
                 *************************************/
                // eg. formSubmit("Password Reset");
                LogService.prototype.formSubmit = function (label) {
                    this.track("Form", "Submit", label);
                };
                LogService.prototype.emailSend = function (label) {
                    this.track("Email", "Send", label);
                };
                // eg. account("Reset", "Password");
                LogService.prototype.account = function (action, label) {
                    this.track("Account", action, label);
                };
                /*************************************
                 * PAGE VIEW
                 *************************************/
                LogService.prototype.pageView = function (page, properties) {
                    if (properties === void 0) { properties = {}; }
                    // GOOGLE
                    ga('set', 'page', page);
                    ga('send', 'pageview');
                    // CUSTOM
                    this.createLog({
                        category: "Page",
                        action: "Visit",
                        label: page
                    });
                    // MIXPANEL
                    mixpanel.track("Visit " + page + " page");
                };
                /*************************************
                 * LINK/Button CLICK
                 *************************************/
                LogService.prototype.linkClick = function (label, properties) {
                    if (properties === void 0) { properties = {}; }
                    this.click(label, "Link", properties);
                };
                LogService.prototype.buttonClick = function (label, properties) {
                    if (properties === void 0) { properties = {}; }
                    this.click(label, "Button", properties);
                };
                LogService.prototype.click = function (label, category, properties) {
                    // GOOGLE
                    ga('send', {
                        hitType: 'event',
                        eventCategory: category,
                        eventAction: 'Click',
                        eventLabel: label
                    });
                    // CUSTOM
                    var data = {
                        category: category,
                        action: "Click",
                        label: label
                    };
                    this.createLog(data);
                    // MIXPANEL
                    var name = "Click " + label + " Link";
                    mixpanel.track(name, properties);
                };
                LogService.prototype.register = function (id) {
                    // MIXPANEL
                    mixpanel.alias(id);
                    // CUSTOM
                    this.mergeSessionTokenLogsWithUser();
                };
                LogService.prototype.identify = function () {
                    id = this._config.getUserId();
                    // MIXPANEL
                    mixpanel.identify(id);
                    // GOOGLE
                    ga('create', this._config.ENV.GA_KEY, 'auto', { userId: id });
                    // CUSTOM
                    this.mergeSessionTokenLogsWithUser();
                };
                LogService.prototype.setProperties = function (properties) {
                    if (properties === void 0) { properties = {}; }
                    // MIXPANEL
                    mixpanel.register(properties);
                };
                LogService.prototype.setPropertiesOnce = function (properties) {
                    if (properties === void 0) { properties = {}; }
                    // MIXPANEL
                    mixpanel.register_once(properties);
                };
                LogService.prototype.profileSet = function (properties) {
                    if (properties === void 0) { properties = {}; }
                    // MIXPANEL
                    mixpanel.people.set(properties);
                };
                LogService.prototype.profileIncrement = function (name, number) {
                    // MIXPANEL
                    mixpanel.people.increment(name, number = 1);
                };
                // CUSTOM
                LogService.prototype.createLog = function (data) {
                    if (data === void 0) { data = {}; }
                    data["user_id"] = this._config.getUserId();
                    data["session_token"] = this._config.getSessionToken();
                    new log_1.Log(data).save();
                };
                // Check for session
                // if not logged in and no session token
                // in local host, request session token
                // Session token allows us to connect logs from
                // non logged in users to the user when they log in
                LogService.prototype.updateLogSessionToken = function () {
                    var userLoggedIn = this._config.getUserId();
                    var sessionToken = this._config.getSessionToken();
                    if ((userLoggedIn && userLoggedIn !== "null") ||
                        (sessionToken && sessionToken !== "null"))
                        return;
                    this._ajaxService.send({
                        path: "/log-session-token",
                        action: "POST"
                    })
                        .then(this.setSessionToken)
                        .catch(this.handleSessionTokenSetFailure);
                };
                LogService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [config_service_1.ConfigService, ajax_service_1.AjaxService])
                ], LogService);
                return LogService;
            })();
            exports_1("LogService", LogService);
        }
    }
});
//# sourceMappingURL=log.service.js.map