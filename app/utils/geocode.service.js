System.register(['angular2/core', '../config/config.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, config_service_1;
    var GeocodeService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            }],
        execute: function() {
            GeocodeService = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function GeocodeService(_config) {
                    var _this = this;
                    this._config = _config;
                    this.prepareData = function () {
                        _this.parsedData = {
                            latitude: _this.lat,
                            longitude: _this.lng,
                            geocodedNumber: _this.geocodedNumber,
                            geocodedStreet: _this.geocodedStreet,
                            geocodedCity: _this.geocodedCity,
                            geocodedState: _this.geocodedState,
                            geocodedStateShort: _this.geocodedStateShort,
                            geocodedCountry: _this.geocodedCountry,
                            geocodedCountryShort: _this.geocodedCountryShort,
                            geocodedZipcode: _this.geocodedZipcode
                        };
                    };
                    this.parseResults = function (results) {
                        _this.parseAddress(results[0].address_components);
                        _this.parseLatLng(results[0].geometry.location);
                    };
                    this.parseLatLng = function (loc) {
                        _this.lat = loc.lat();
                        _this.lng = loc.lng();
                    };
                    this.parseAddress = function (components) {
                        var self = _this;
                        components.forEach(function (address_component) {
                            var name = address_component.long_name;
                            var shortName = address_component.short_name;
                            switch (address_component.types[0]) {
                                case "route":
                                    self.geocodedStreet = name;
                                    break;
                                case "locality":
                                    self.geocodedCity = name;
                                    break;
                                case "country":
                                    self.geocodedCountry = name;
                                    self.geocodedCountryShort = shortName;
                                    break;
                                case "postal_code":
                                    self.geocodedZipcode = name;
                                    break;
                                case "street_number":
                                    self.geocodedNumber = name;
                                    break;
                                case "administrative_area_level_1":
                                    self.geocodedStateShort = shortName;
                                    self.geocodedState = name;
                                    break;
                            }
                        });
                    };
                }
                /*************************************
                 * METHODS
                 *************************************/
                GeocodeService.prototype.initialize = function () {
                    var key = this._config.ENV.GOOGLE_MAP_KEY;
                    $.ajax({
                        url: "http://maps.googleapis.com/maps/api/js?v=3&key=" + key,
                        dataType: "script",
                        timeout: 8000,
                        error: function (err) {
                            console.log("google maps couldn't load: " + err);
                        }
                    });
                };
                GeocodeService.prototype.geocode = function (address) {
                    var self = this;
                    var geocoder = new google.maps.Geocoder();
                    return new Promise(function (resolve, reject) {
                        geocoder.geocode({ 'address': address }, function (results, status) {
                            if (status === google.maps.GeocoderStatus.OK) {
                                self.parseResults(results);
                                self.prepareData();
                                return resolve(self.parsedData);
                            }
                            else {
                                return reject({ geocodeError: status });
                            }
                        });
                    });
                };
                ;
                GeocodeService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [config_service_1.ConfigService])
                ], GeocodeService);
                return GeocodeService;
            })();
            exports_1("GeocodeService", GeocodeService);
        }
    }
});
//# sourceMappingURL=geocode.service.js.map