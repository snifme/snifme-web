System.register(['angular2/core', './session.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, session_service_1;
    var CurrentUserService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (session_service_1_1) {
                session_service_1 = session_service_1_1;
            }],
        execute: function() {
            CurrentUserService = (function () {
                /*************************************
                 * PROPERTIES
                 *************************************/
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function CurrentUserService(session) {
                    this.session = session;
                }
                /*************************************
                 * METHODS
                 *************************************/
                CurrentUserService.prototype.getInfo = function () {
                    var self = this;
                    var session = this.session.getSession();
                    if (session) {
                        return {
                            id: session.user_id,
                            email: session.user_email,
                            firstName: session.first_name,
                            lastName: session.last_name
                        };
                    }
                    else {
                        return null;
                    }
                };
                CurrentUserService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [session_service_1.SessionService])
                ], CurrentUserService);
                return CurrentUserService;
            })();
            exports_1("CurrentUserService", CurrentUserService);
        }
    }
});
//# sourceMappingURL=current-user.service.js.map