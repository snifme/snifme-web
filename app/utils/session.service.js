System.register(['angular2/core', 'angular2/router', '../models/logs/log.service', './ajax.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, log_service_1, ajax_service_1;
    var SessionService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (log_service_1_1) {
                log_service_1 = log_service_1_1;
            },
            function (ajax_service_1_1) {
                ajax_service_1 = ajax_service_1_1;
            }],
        execute: function() {
            SessionService = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function SessionService(_router, log, _ajaxService) {
                    var _this = this;
                    this._router = _router;
                    this.log = log;
                    this._ajaxService = _ajaxService;
                    this.getSession = function () {
                        if (localStorage.session) {
                            return JSON.parse(localStorage.session);
                        }
                    };
                    this.getToken = function () {
                        var session = _this.getSession();
                        if (session) {
                            return session.token;
                        }
                    };
                    this.destroyToken = function () {
                        _this._ajaxService.send({
                            data: JSON.stringify({
                                token: _this.getToken()
                            }),
                            path: 'sessions-delete',
                            action: 'DELETE'
                        });
                    };
                }
                SessionService.prototype.redirectIfLoggedIn = function () {
                    if (this.signedIn()) {
                        this.log.track('Authentication', 'Redirect', 'Trying to access logged out page when logged in');
                        this._router.navigate(['Home'], { alreadyLoggedIn: "true" });
                    }
                };
                SessionService.prototype.redirectIfNotLoggedIn = function () {
                    if (!this.signedIn()) {
                        this.log.track('Authentication', 'Redirect', 'Trying to access logged in page when logged out');
                        this._router.navigate(['Login', { unauthorized: "true" }]);
                    }
                };
                SessionService.prototype.signedIn = function () {
                    var token = this.getToken();
                    if (token && token.length > 0) {
                        return true;
                    }
                    else {
                        return false;
                    }
                };
                SessionService.prototype.logOut = function () {
                    $('.dropdown-toggle').trigger('click');
                    this.log.account("Logout");
                    this.destroyToken();
                    localStorage.session = null;
                    this.log.updateLogSessionToken();
                    $("#session-check").trigger('click');
                    window.loggingOut = true;
                };
                SessionService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [router_1.Router, log_service_1.LogService, ajax_service_1.AjaxService])
                ], SessionService);
                return SessionService;
            })();
            exports_1("SessionService", SessionService);
        }
    }
});
//# sourceMappingURL=session.service.js.map