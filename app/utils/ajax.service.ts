import {Injectable} from 'angular2/core';
import {ConfigService} from '../config/config.service';

@Injectable()

export class AjaxService {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private _config: ConfigService
  ) {}

  // methods
  send(options) {
    const _this = this;
    const baseUrl = options.baseUrl || this._config.API_URL;
    const endpoint = options.path || "";
    const action = options.action || "GET";
    const data = options.data || {};


    return new Promise((resolve,reject) => {

      let headers = {
        Accept: "application/vnd.snifme.v1",
        "Content-Type": "application/json"
      };

      if (localStorage.session &&
          JSON.parse(localStorage.session) &&
          JSON.parse(localStorage.session).token &&
          JSON.parse(localStorage.session).token.length > 0) {
        headers["Authentication"] = JSON.parse(localStorage.session).token;
      }

      $.ajax({
        url: baseUrl + '/' + endpoint,
        headers: headers,
        data: data,
        method: action
      })
      .done((response) => {
        return resolve(response);
      })
      .fail((err) => {
        return reject(err);
      });
    });

  }

}
