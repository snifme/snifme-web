System.register(['angular2/core', 'angular2/router', '../models/logs/log.service', '../config/config.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, log_service_1, config_service_1;
    var FacebookService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (log_service_1_1) {
                log_service_1 = log_service_1_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            }],
        execute: function() {
            FacebookService = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function FacebookService(_router, log, _config) {
                    var _this = this;
                    this._router = _router;
                    this.log = log;
                    this._config = _config;
                    this.facebookInfo = [
                        'age_range',
                        'about',
                        'bio',
                        'birthday',
                        'context',
                        'devices',
                        'education',
                        'email',
                        'first_name',
                        'gender',
                        'hometown',
                        'interested_in',
                        'is_verified',
                        'languages',
                        'last_name',
                        'link',
                        'location',
                        'middle_name',
                        'name_format',
                        'political',
                        'relationship_status',
                        'religion',
                        'significant_other',
                        'timezone',
                        //'token_for_business',
                        'updated_time',
                        'verified',
                        'website',
                        'work',
                        'cover',
                        'public_key',
                        'picture'
                    ].join(',');
                    this.isConnected = function (state) {
                        if (state && state.status &&
                            state.status === "connected") {
                            _this.loginState = state;
                            return state.authResponse.accessToken;
                        }
                        else {
                            return false;
                        }
                    };
                }
                /*************************************
                 * METHODS
                 *************************************/
                FacebookService.prototype.connect = function () {
                    var _this = this;
                    var data;
                    var token = this.isConnected(_this.loginState);
                    if (token) {
                        return _this.fetchData(token);
                    }
                    else {
                        return new Promise(function (resolve, reject) {
                            FB.login(function (response) {
                                token = _this.isConnected(response);
                                if (token) {
                                    _this.fetchData(token)
                                        .then(resolve)
                                        .catch(reject);
                                }
                                else {
                                    return reject();
                                }
                            }, { scope: 'email' });
                        });
                    }
                };
                FacebookService.prototype.fetchData = function (accessToken) {
                    var _this = this;
                    var token = accessToken;
                    var _this = this;
                    return new Promise(function (resolve, reject) {
                        FB.api('/me?fields=' + _this.facebookInfo, function (response) {
                            if (response.error)
                                reject(response.error);
                            response.token = token;
                            resolve(response);
                        });
                    });
                };
                FacebookService.prototype.initialize = function () {
                    var _this = this;
                    $(document).ready(function () {
                        window.fbAsyncInit = function () {
                            FB.init({
                                appId: _this._config.ENV.FACEBOOK_KEY,
                                xfbml: true,
                                version: 'v2.5'
                            });
                        };
                        setTimeout(function () {
                            FB.getLoginStatus(function (response) {
                                _this.loginState = response;
                            });
                        }, 500);
                        (function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) {
                                return;
                            }
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/en_US/sdk.js";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));
                    });
                };
                FacebookService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [router_1.Router, log_service_1.LogService, config_service_1.ConfigService])
                ], FacebookService);
                return FacebookService;
            })();
            exports_1("FacebookService", FacebookService);
        }
    }
});
//# sourceMappingURL=facebook.service.js.map