import {Injectable} from 'angular2/core';
import {Router} from 'angular2/router';
import {LogService} from '../models/logs/log.service';
import {ConfigService} from '../config/config.service';

@Injectable()

export class FacebookService {

  /*************************************
   * PROPERTIES
   *************************************/
  loginState
  token
  facebookInfo = [
    'age_range',
    'about',
    'bio',
    'birthday',
    'context',
    'devices',
    'education',
    'email',
    'first_name',
    'gender',
    'hometown',
    'interested_in',
    'is_verified',
    'languages',
    'last_name',
    'link',
    'location',
    'middle_name',
    'name_format',
    'political',
    'relationship_status',
    'religion',
    'significant_other',
    'timezone',
    //'token_for_business',
    'updated_time',
    'verified',
    'website',
    'work',
    'cover',
    'public_key',
    'picture'
  ].join(',');

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private _router: Router,
    private log: LogService,
    private _config: ConfigService
  ){}

  /*************************************
   * METHODS
   *************************************/
  connect() {
    const _this = this;
    let data;
    let token = this.isConnected(_this.loginState)
    if (token) {
       return _this.fetchData(token);
    } else {
      return new Promise((resolve, reject) => {
        FB.login((response) => {
          token =  _this.isConnected(response)
          if (token) {
            _this.fetchData(token)
              .then(resolve)
              .catch(reject);
          } else {
            return reject();
          }
        }, {scope: 'email'});
      }
    }
  }

  private isConnected = (state) => {
    if (state && state.status &&
        state.status === "connected") {
      this.loginState = state;
      return state.authResponse.accessToken;
    } else {
      return false;
    }
  }

  fetchData(accessToken) {
    const token = accessToken
    const _this = this;
    return new Promise((resolve,reject) => {
      FB.api('/me?fields='+ this.facebookInfo, (response) => {
        if (response.error) reject(response.error);
        response.token = token;
        resolve(response);
      });
    });
  }

  initialize() {
    const _this = this;
    $(document).ready(function() {
      window.fbAsyncInit = function() {
        FB.init({
          appId      : _this._config.ENV.FACEBOOK_KEY,
          xfbml      : true,
          version    : 'v2.5'
        });
      };

      setTimeout(() => {
        FB.getLoginStatus((response) => {
          _this.loginState = response;
        });
      }, 500);

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
    });
  }
}
