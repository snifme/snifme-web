import {Injectable} from 'angular2/core';
import {Router} from 'angular2/router';
import {LogService} from '../models/logs/log.service';
import {AjaxService} from './ajax.service';

@Injectable()

export class SessionService {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
    private _router: Router,
    private log: LogService,
    private _ajaxService:AjaxService
  ){}

  redirectIfLoggedIn() {
    if (this.signedIn()) {
      this.log.track('Authentication', 'Redirect', 'Trying to access logged out page when logged in')
      this._router.navigate(['Home'], {alreadyLoggedIn:"true"});
    }
  }

  redirectIfNotLoggedIn() {
    if (!this.signedIn()) {
      this.log.track('Authentication', 'Redirect', 'Trying to access logged in page when logged out')
      this._router.navigate(['Login', {unauthorized:"true"}]);
    }
  }


  signedIn() {
    const token = this.getToken();
    if (token && token.length > 0) {
      return true;
    } else {
      return false;
    }
  }

  private getSession = () => {
    if(localStorage.session) {
      return JSON.parse(localStorage.session);
    }
  }

  private getToken = () => {
    const session = this.getSession();
    if (session) {
      return session.token;
    }
  }

  private destroyToken = () => {
    this._ajaxService.send({
      data: JSON.stringify({
        token: this.getToken()
      }),
      path: 'sessions-delete',
      action: 'DELETE'
    });
  }

  logOut() {
    $('.dropdown-toggle').trigger('click');
    this.log.account("Logout");
    this.destroyToken();
    localStorage.session = null;
    this.log.updateLogSessionToken();
    $("#session-check").trigger('click');
    window.loggingOut = true;
  }
}
