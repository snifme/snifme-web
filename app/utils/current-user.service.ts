import {Injectable} from 'angular2/core';
import {SessionService} from './session.service';

@Injectable()

export class CurrentUserService {

  /*************************************
   * PROPERTIES
   *************************************/

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
      private session: SessionService
  ) {}

  /*************************************
   * METHODS
   *************************************/
  getInfo() {
    const self = this;
    const session = this.session.getSession();
    if (session) {
      return {
        id: session.user_id,
        email: session.user_email,
        firstName: session.first_name,
        lastName: session.last_name
      };
    } else {
      return null;
    }
  }

}
