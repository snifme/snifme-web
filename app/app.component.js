System.register(['angular2/core', 'angular2/router', './config/env.service', './config/config.service', './utils/ajax.service', './utils/facebook.service', './utils/session.service', './utils/geocode.service', './utils/current-user.service', './models/logs/log.service', './models/users/user-detail.component', './index.component', './pages/account/register/register.component', './pages/account/login/login.component', './pages/account/reconfirm/reconfirm.component', './pages/account/password-reset-request/password-reset-request.component', './pages/account/facebook-connect/facebook-connect.component', './pages/account/password-reset/password-reset.component', './pages/secure/home/home.component', './pages/secure/settings/settings.component', './pages/secure/account/dog-owner/new/new-dog-owner'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, env_service_1, config_service_1, ajax_service_1, facebook_service_1, session_service_1, geocode_service_1, current_user_service_1, log_service_1, user_detail_component_1, index_component_1, register_component_1, login_component_1, reconfirm_component_1, password_reset_request_component_1, facebook_connect_component_1, password_reset_component_1, home_component_1, settings_component_1, new_dog_owner_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (env_service_1_1) {
                env_service_1 = env_service_1_1;
            },
            function (config_service_1_1) {
                config_service_1 = config_service_1_1;
            },
            function (ajax_service_1_1) {
                ajax_service_1 = ajax_service_1_1;
            },
            function (facebook_service_1_1) {
                facebook_service_1 = facebook_service_1_1;
            },
            function (session_service_1_1) {
                session_service_1 = session_service_1_1;
            },
            function (geocode_service_1_1) {
                geocode_service_1 = geocode_service_1_1;
            },
            function (current_user_service_1_1) {
                current_user_service_1 = current_user_service_1_1;
            },
            function (log_service_1_1) {
                log_service_1 = log_service_1_1;
            },
            function (user_detail_component_1_1) {
                user_detail_component_1 = user_detail_component_1_1;
            },
            function (index_component_1_1) {
                index_component_1 = index_component_1_1;
            },
            function (register_component_1_1) {
                register_component_1 = register_component_1_1;
            },
            function (login_component_1_1) {
                login_component_1 = login_component_1_1;
            },
            function (reconfirm_component_1_1) {
                reconfirm_component_1 = reconfirm_component_1_1;
            },
            function (password_reset_request_component_1_1) {
                password_reset_request_component_1 = password_reset_request_component_1_1;
            },
            function (facebook_connect_component_1_1) {
                facebook_connect_component_1 = facebook_connect_component_1_1;
            },
            function (password_reset_component_1_1) {
                password_reset_component_1 = password_reset_component_1_1;
            },
            function (home_component_1_1) {
                home_component_1 = home_component_1_1;
            },
            function (settings_component_1_1) {
                settings_component_1 = settings_component_1_1;
            },
            function (new_dog_owner_1_1) {
                new_dog_owner_1 = new_dog_owner_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function AppComponent(log, _session, _router, _facebook, _config, geocode) {
                    this.log = log;
                    this._session = _session;
                    this._router = _router;
                    this._facebook = _facebook;
                    this._config = _config;
                    this.geocode = geocode;
                    /*************************************
                     * PROPERTIES
                     *************************************/
                    this.title = 'Snifme';
                    this.signedIn = false;
                }
                /*************************************
                 * INITIALIZER
                 *************************************/
                AppComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    // initialize material
                    $.material.init();
                    // initialize mixpanel
                    mixpanel.init(_this._config.ENV.MIXPANEL_KEY);
                    // initialize google analytics
                    id = this._config.getUserId();
                    if (id) {
                        ga('create', this._config.ENV.GA_KEY, 'auto', { userId: id });
                    }
                    else {
                        ga('create', this._config.ENV.GA_KEY, 'auto');
                    }
                    // initialize google maps
                    this.geocode.initialize();
                    // check session
                    this.checkSession(this);
                    $('#session-check').on('click', function () {
                        _this.checkSession(_this);
                    });
                    this._facebook.initialize();
                    this.log.updateLogSessionToken();
                };
                /*************************************
                 * METHODS
                 *************************************/
                AppComponent.prototype.logout = function () {
                    this.log.linkClick('Nav Logout');
                    this._session.logOut();
                };
                AppComponent.prototype.checkSession = function (_this) {
                    if (_this._session.signedIn()) {
                        window.signedIn = true;
                        _this.signedIn = true;
                    }
                    else {
                        window.signedIn = false;
                        _this.signedIn = false;
                    }
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'my-app',
                        templateUrl: 'app/app.component.html',
                        directives: [user_detail_component_1.UserDetailComponent, router_1.ROUTER_DIRECTIVES],
                        providers: [
                            ajax_service_1.AjaxService, log_service_1.LogService, session_service_1.SessionService,
                            config_service_1.ConfigService, env_service_1.EnvService, facebook_service_1.FacebookService,
                            facebook_connect_component_1.FacebookConnectComponent, geocode_service_1.GeocodeService,
                            current_user_service_1.CurrentUserService
                        ]
                    }),
                    router_1.RouteConfig([
                        { path: '/', name: 'Index', component: index_component_1.IndexComponent, useAsDefault: true },
                        { path: '/register', name: 'Register', component: register_component_1.RegisterComponent },
                        { path: '/login', name: 'Login', component: login_component_1.LoginComponent },
                        { path: '/reconfirm', name: 'Reconfirm', component: reconfirm_component_1.ReconfirmComponent },
                        { path: '/password-reset-email', name: 'PasswordResetRequest',
                            component: password_reset_request_component_1.PasswordResetRequestComponent },
                        { path: '/password-reset', name: 'PasswordReset',
                            component: password_reset_component_1.PasswordResetComponent },
                        { path: '/home', name: 'Home', component: home_component_1.HomeComponent },
                        { path: '/settings', name: 'Settings', component: settings_component_1.SettingsComponent },
                        { path: '/new-dog-owner', name: 'NewDogOwner', component: new_dog_owner_1.NewDogOwnerPage }
                    ]), 
                    __metadata('design:paramtypes', [log_service_1.LogService, session_service_1.SessionService, router_1.Router, facebook_service_1.FacebookService, config_service_1.ConfigService, geocode_service_1.GeocodeService])
                ], AppComponent);
                return AppComponent;
            })();
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map