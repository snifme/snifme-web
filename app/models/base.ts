import {Component} from 'angular2/core';
import {AjaxService} from '../utils/ajax.service';
import {EnvService} from '../config/env.service';

export class Base {

  constructor(data,target) {
    this.ajax = new AjaxService;
    this.env = new EnvService;
    target.prepareKeys();
    target.copyProperties(data);
  }

  // methods
  copyProperties(data:any):void {
    for(let prop in data){
      if(this[prop] !== undefined){
          this[prop] = data[prop];
      }
      else {
          console.error("Cannot set undefined property: " + prop);
      }
    }
  }

  prepareKeys() {
    let _this = this;
    _this.keys.forEach((key) => {
      _this[key] = null;
    });
  }

  save(options) {
    const _this = this;
    return Promise.resolve()
    .then(() => {
      let className = this.constructor.name.toLowerCase();
      const data =  _this.prepareData(className);
      const options = {
        data: data,
        baseUrl: _this.env.API_URL,
        path: className + 's',
        action: _this.id ? 'PATCH' : 'POST'
      };

      return _this.ajax.send(options);
    });
  }

  toJSON() {
    let _this = this;
    let json = {};
    this.keys.forEach((key) => {
      json[key] = _this[key];
    });
    return json;
  }

  prepareData(className) {
    let data = {};
    data[className] = this.toJSON();
    return JSON.stringify(data);
  }
}
