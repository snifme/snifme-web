System.register(['../base'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var base_1;
    var Log;
    return {
        setters:[
            function (base_1_1) {
                base_1 = base_1_1;
            }],
        execute: function() {
            Log = (function (_super) {
                __extends(Log, _super);
                function Log(data) {
                    this.keys = [
                        "name",
                        "category",
                        "action",
                        "label",
                        "session_token",
                        "description",
                        "user_id",
                        "error_name",
                        "platform",
                        "device_info",
                        "languages",
                        "language"
                    ];
                    _super.call(this, data, this);
                }
                return Log;
            })(base_1.Base);
            exports_1("Log", Log);
        }
    }
});
//# sourceMappingURL=log.js.map