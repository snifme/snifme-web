import {Injectable} from 'angular2/core';
import {Log}        from './log';
import {ConfigService} from '../../config/config.service';
import {AjaxService} from '../../utils/ajax.service';

@Injectable()

export class LogService {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
      private _config: ConfigService,
      private _ajaxService: AjaxService
  ) {}

  /*************************************
   * GENERAL EVENT TRACK
   *************************************/
  track(category,action,label="",properties={}) {

    // GOOGLE
    ga('send', {
        hitType: 'event',
        eventCategory: category,
        eventAction: action,
        eventLabel: label
      }
    );

    // CUSTOM
    const data = {
      category: category,
      action: action,
      label: label
    };
    this.createLog(data);

    // MIXPANEL
    if (category == "Error") {
      mixpanel.track("Error", {
        "Error Category": action,
        "Error Info": label
      })
    } else {
      const name = category + " " + action + " " + label;
      mixpanel.track(name, properties);
    }
   }

  /*************************************
   * CUSTOM EVENTS
   *************************************/
  // eg. formSubmit("Password Reset");
  formSubmit(label) {
    this.track("Form", "Submit", label);
  }

  emailSend(label) {
    this.track("Email", "Send", label);
  }

  // eg. account("Reset", "Password");
  account(action, label) {
    this.track("Account", action, label);
  }

  /*************************************
   * PAGE VIEW
   *************************************/
  pageView(page, properties={}) {

    // GOOGLE
    ga('set', 'page', page);
    ga('send', 'pageview');

    // CUSTOM
    this.createLog({
      category: "Page",
      action: "Visit",
      label: page
    });

    // MIXPANEL
    mixpanel.track("Visit " + page + " page");
  }

  /*************************************
   * LINK/Button CLICK
   *************************************/
   linkClick(label, properties={}) {
     this.click(label, "Link", properties);
   }

   buttonClick(label, properties={}) {
     this.click(label, "Button", properties);
   }

   click(label, category, properties) {

    // GOOGLE
    ga('send', {
        hitType: 'event',
        eventCategory: category,
        eventAction: 'Click',
        eventLabel: label
      }
    );

    // CUSTOM
    const data = {
      category: category,
      action: "Click",
      label: label
    };
    this.createLog(data);

    // MIXPANEL
    const name = "Click " + label + " Link";
    mixpanel.track(name,properties);
   }

  /*************************************
   * ERROR
   *************************************/
  public error = (action, label="") => {

    if (typeof label !== "string") {
     label = JSON.stringify(label);
    }

    // send event track
    this.track("Error", action, label);

    // GOOGLE
    ga('send', 'exception', {
      'exDescription': action + " " + label,
      'exFatal': false
    });

  }

  register(id) {
    // MIXPANEL
    mixpanel.alias(id);

    // CUSTOM
    this.mergeSessionTokenLogsWithUser();
  }

  identify() {
    id = this._config.getUserId();
    // MIXPANEL
    mixpanel.identify(id);

    // GOOGLE
    ga('create', this._config.ENV.GA_KEY, 'auto', {userId: id});

    // CUSTOM
    this.mergeSessionTokenLogsWithUser();
  }

  setProperties(properties={}) {
    // MIXPANEL
    mixpanel.register(properties);
  }

  setPropertiesOnce(properties={}) {
    // MIXPANEL
    mixpanel.register_once(properties);
  }

  profileSet(properties={}) {
    // MIXPANEL
    mixpanel.people.set(properties);
  }

  profileIncrement(name,number?) {
    // MIXPANEL
    mixpanel.people.increment(name,number=1);
  }

  // CUSTOM
  createLog(data={}) {
    data["user_id"] = this._config.getUserId();
    data["session_token"] = this._config.getSessionToken();

    if (window && window.navigator) {
      if (window.navigator.platform) {
        data["platform"] = window.navigator.platform;
      }
      if (window.navigator.userAgent) {
        data["device_info"] = window.navigator.userAgent;
      }

      if (window.navigator.languages) {
        data["languages"] = window.navigator.languages;
      }

      if (window.navigator.language) {
        data["language"] = window.navigator.language;
      }
    }

    new Log(data).save();
  }

  private setSessionToken = (response) => {
    localStorage.sessionToken = response.token
  }

  private handleSessionTokenSetFailure = (err) => {
    console.log(err);
    this.error("set", "session token");
  }

  // Check for session
  // if not logged in and no session token
  // in local host, request session token
  // Session token allows us to connect logs from
  // non logged in users to the user when they log in
  public updateLogSessionToken() {
    const userLoggedIn = this._config.getUserId();
    const sessionToken = this._config.getSessionToken();
    if ((userLoggedIn && userLoggedIn !== "null") ||
        (sessionToken && sessionToken !== "null")) return;
    this._ajaxService.send({
      path: "/log-session-token",
      action: "POST"
    })
    .then(this.setSessionToken)
    .catch(this.handleSessionTokenSetFailure);
  }

  private removeSessionToken = (response) => {
    localStorage.sessionToken = null;
  }

  private handleSessionTokenMergeFailure = (err) => {
    console.log(err);
    this.error("merge","session token");
  }

  private mergeSessionTokenLogsWithUser = () => {
    const userId = this._config.getUserId();
    const sessionToken = this._config.getSessionToken();
    this._ajaxService.send({
      path: "/merge-log-session-token",
      action: "PATCH",
      data: JSON.stringify({
        "session_token": sessionToken,
        "user_id": userId
      })
    })
    .then(this.removeSessionToken)
    .catch(this.handleSessionTokenMergeFailure);
  }
}
