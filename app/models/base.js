System.register(['../utils/ajax.service', '../config/env.service'], function(exports_1) {
    var ajax_service_1, env_service_1;
    var Base;
    return {
        setters:[
            function (ajax_service_1_1) {
                ajax_service_1 = ajax_service_1_1;
            },
            function (env_service_1_1) {
                env_service_1 = env_service_1_1;
            }],
        execute: function() {
            Base = (function () {
                function Base(data, target) {
                    this.ajax = new ajax_service_1.AjaxService;
                    this.env = new env_service_1.EnvService;
                    target.prepareKeys();
                    target.copyProperties(data);
                }
                // methods
                Base.prototype.copyProperties = function (data) {
                    for (var prop in data) {
                        if (this[prop] !== undefined) {
                            this[prop] = data[prop];
                        }
                        else {
                            console.error("Cannot set undefined property: " + prop);
                        }
                    }
                };
                Base.prototype.prepareKeys = function () {
                    var _this = this;
                    _this.keys.forEach(function (key) {
                        _this[key] = null;
                    });
                };
                Base.prototype.save = function (options) {
                    var _this = this;
                    var _this = this;
                    return Promise.resolve()
                        .then(function () {
                        var className = _this.constructor.name.toLowerCase();
                        var data = _this.prepareData(className);
                        var options = {
                            data: data,
                            baseUrl: _this.env.API_URL,
                            path: className + 's',
                            action: _this.id ? 'PATCH' : 'POST'
                        };
                        return _this.ajax.send(options);
                    });
                };
                Base.prototype.toJSON = function () {
                    var _this = this;
                    var json = {};
                    this.keys.forEach(function (key) {
                        json[key] = _this[key];
                    });
                    return json;
                };
                Base.prototype.prepareData = function (className) {
                    var data = {};
                    data[className] = this.toJSON();
                    return JSON.stringify(data);
                };
                return Base;
            })();
            exports_1("Base", Base);
        }
    }
});
//# sourceMappingURL=base.js.map