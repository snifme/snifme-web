System.register(['../base'], function(exports_1) {
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var base_1;
    var User;
    return {
        setters:[
            function (base_1_1) {
                base_1 = base_1_1;
            }],
        execute: function() {
            User = (function (_super) {
                __extends(User, _super);
                function User(data) {
                    this.keys = [
                        "id",
                        "email",
                        "password"
                    ];
                    _super.call(this, data, this);
                }
                return User;
            })(base_1.Base);
            exports_1("User", User);
        }
    }
});
//# sourceMappingURL=user.js.map