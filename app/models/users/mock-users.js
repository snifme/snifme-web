System.register([], function(exports_1) {
    var USERS;
    return {
        setters:[],
        execute: function() {
            exports_1("USERS", USERS = [
                {
                    "id": 1,
                    "first_name": 'Monty',
                    "last_name": 'Lennie'
                },
                {
                    "id": 2,
                    "first_name": 'Jimbo',
                    "last_name": 'Bob'
                },
                {
                    "id": 3,
                    "first_name": 'Mr.',
                    "last_name": 'Magoo'
                },
                {
                    "id": 4,
                    "first_name": 'Oscar',
                    "last_name": 'Maximus'
                }
            ]);
        }
    }
});
//# sourceMappingURL=mock-users.js.map