import {Injectable} from 'angular2/core';
import {USERS} from './mock-users';

@Injectable()

export class UserService {

  // properties

  // methods
  getUsers() {
    return Promise.resolve(USERS);
  }
}
