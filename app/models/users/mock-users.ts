import {User} from './user';

export const USERS: User[] = [
  {
    "id": 1,
    "first_name": 'Monty',
    "last_name": 'Lennie'
  },
 {
    "id": 2,
    "first_name": 'Jimbo',
    "last_name": 'Bob'
  },

 {
    "id": 3,
    "first_name": 'Mr.',
    "last_name": 'Magoo'
  },

 {
    "id": 4,
    "first_name": 'Oscar',
    "last_name": 'Maximus'
  }

];
