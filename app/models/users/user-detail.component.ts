import {Component} from 'angular2/core';
import {User} from './user';

@Component({
  selector: 'user-detail',
  templateUrl: 'app/models/users/user-detail.component.html',
  inputs: ['user']
})

export class UserDetailComponent {

  // properties
  public user: User;
}
