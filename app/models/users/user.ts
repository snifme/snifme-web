import {Base} from '../base';

export class User extends Base {

  constructor(data) {
    this.keys = [
      "id",
      "email",
      "password"
    ];
    super(data,this);
  }
}
