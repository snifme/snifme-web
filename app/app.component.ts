// ANGULER
import {Component,OnInit} from 'angular2/core';
import {RouteConfig, Router, ROUTER_DIRECTIVES} from 'angular2/router';

// CONFIG
import {EnvService} from './config/env.service';
import {ConfigService} from './config/config.service';

// UTILS
import {AjaxService} from './utils/ajax.service';
import {FacebookService} from './utils/facebook.service';
import {SessionService} from './utils/session.service';
import {GeocodeService} from './utils/geocode.service';
import {CurrentUserService} from './utils/current-user.service';

// MODELS
import {User} from './models/users/user';
import {LogService} from './models/logs/log.service';
import {UserDetailComponent} from './models/users/user-detail.component';

// PAGES
// Account
import {IndexComponent} from './index.component';
import {RegisterComponent} from './pages/account/register/register.component';
import {LoginComponent} from './pages/account/login/login.component';
import {ReconfirmComponent} from './pages/account/reconfirm/reconfirm.component';
import {PasswordResetRequestComponent} from './pages/account/password-reset-request/password-reset-request.component';
import {FacebookConnectComponent} from './pages/account/facebook-connect/facebook-connect.component';
import {PasswordResetComponent} from './pages/account/password-reset/password-reset.component';
// Secure
import {HomeComponent} from './pages/secure/home/home.component';
import {SettingsComponent} from './pages/secure/settings/settings.component';
import {NewDogOwnerPage} from './pages/secure/account/dog-owner/new/new-dog-owner';

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    directives: [UserDetailComponent,ROUTER_DIRECTIVES],
    providers: [
      AjaxService,LogService,SessionService,
      ConfigService,EnvService,FacebookService,
      FacebookConnectComponent,GeocodeService,
      CurrentUserService
    ]
})

@RouteConfig([
  {path: '/',        name: 'Index', component: IndexComponent, useAsDefault: true},
  {path:'/register', name: 'Register', component: RegisterComponent},
  {path: '/login',   name: 'Login', component: LoginComponent},
  {path: '/reconfirm', name: 'Reconfirm', component: ReconfirmComponent},
  {path: '/password-reset-email', name: 'PasswordResetRequest',
   component: PasswordResetRequestComponent},
  {path: '/password-reset', name: 'PasswordReset',
   component: PasswordResetComponent},
  {path: '/home', name: 'Home', component: HomeComponent},
  {path: '/settings', name: 'Settings', component: SettingsComponent},
  {path: '/new-dog-owner', name: 'NewDogOwner', component: NewDogOwnerPage}

])

export class AppComponent implements OnInit {

  /*************************************
   * CONSTRUCTOR
   *************************************/
  constructor(
      private log: LogService,
      private _session: SessionService,
      private _router: Router,
      private _facebook: FacebookService,
      private _config: ConfigService,
      private geocode: GeocodeService
  ) {}

  /*************************************
   * PROPERTIES
   *************************************/
  title = 'Snifme';
  signedIn: boolean = false;

  /*************************************
   * INITIALIZER
   *************************************/
  ngOnInit() {
    const _this = this;
    // initialize material
    $.material.init()
    // initialize mixpanel
    mixpanel.init(_this._config.ENV.MIXPANEL_KEY);
    // initialize google analytics
    id = this._config.getUserId();
    if (id) {
      ga('create', this._config.ENV.GA_KEY, 'auto', {userId: id});
    } else {
      ga('create', this._config.ENV.GA_KEY, 'auto');
    }
    // initialize google maps
    this.geocode.initialize();

    // check session
    this.checkSession(this);
    $('#session-check').on('click', () => {
      _this.checkSession(_this);
    });
    this._facebook.initialize();
    this.log.updateLogSessionToken();
   }

  /*************************************
   * METHODS
   *************************************/
  logout() {
    this.log.linkClick('Nav Logout');
    this._session.logOut();
  }

  checkSession(_this) {
    if (_this._session.signedIn()) {
      window.signedIn = true;
      _this.signedIn = true;
    } else {
      window.signedIn = false;
      _this.signedIn = false;
    }
  }

}

