System.register(['angular2/core', './env.service'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, env_service_1;
    var ConfigService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (env_service_1_1) {
                env_service_1 = env_service_1_1;
            }],
        execute: function() {
            ConfigService = (function () {
                /*************************************
                 * CONSTRUCTOR
                 *************************************/
                function ConfigService(ENV) {
                    this.ENV = ENV;
                    /*************************************
                     * PROPERTIES
                     *************************************/
                    this.WEB_URL = this.ENV.WEB_URL;
                    this.API_URL = this.ENV.API_URL;
                }
                /*************************************
                 * INITIALIZER
                 *************************************/
                ConfigService.prototype.ngOnInit = function () {
                    var environment = this.ENV.NODE_ENV || "development";
                    if (environment === "development") {
                        this.setupDevEnvironment();
                    }
                    if (environment === "test") {
                        this.setupTestEnvironment();
                    }
                    if (environment === "staging") {
                        this.setupStagingEnvironment();
                    }
                    if (environment === "production") {
                        this.setupProductionEnvironment();
                    }
                };
                ConfigService.prototype.getUserId = function () {
                    if (localStorage.session &&
                        JSON.parse(localStorage.session) &&
                        JSON.parse(localStorage.session)["user_id"]) {
                        return JSON.parse(localStorage.session)["user_id"];
                    }
                    return null;
                };
                ConfigService.prototype.getSessionToken = function () {
                    return localStorage.sessionToken;
                };
                ConfigService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [env_service_1.EnvService])
                ], ConfigService);
                return ConfigService;
            })();
            exports_1("ConfigService", ConfigService);
        }
    }
});
//# sourceMappingURL=config.service.js.map